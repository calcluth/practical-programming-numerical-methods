#include"header.h"

void rkstep23(
  double x,                                  /* the current value of the variable */
  double h,                                  /* the step to be taken */
  gsl_vector* yx,                                /* the current value y(t) of the sought function */
  void f(double t, gsl_vector* y, gsl_vector* dydt), /* the right-hand-side, dydt = f(t,y) */
  gsl_vector* yth,                               /* output: y(t+h) */
  gsl_vector* err                                /* output: error estimate dy */
) {

  int i;
  int n = yx->size;

  gsl_vector* yt = gsl_vector_alloc(n);
  gsl_vector* k1 = gsl_vector_alloc(n);
  gsl_vector* k2 = gsl_vector_alloc(n);
  gsl_vector* k3 = gsl_vector_alloc(n);
  gsl_vector* k4 = gsl_vector_alloc(n);

  f(x,yx,k1);

  for (i = 0;i<n;i++)
    gsl_vector_set(yt,i,gsl_vector_get(yx,i)+h/2.0*gsl_vector_get(k1,i));

  f(x+1./2*h,yt,k2);

  for (i = 0;i<n;i++)
    gsl_vector_set(yt,i,gsl_vector_get(yx,i)+3.0*h/4.0*gsl_vector_get(k2,i));
  
  f(x+3./4*h,yt,k3);

  for (i = 0;i<n;i++)
    gsl_vector_set(yth,i,gsl_vector_get(yx,i)+(2.0/9.0*gsl_vector_get(k1,i)+1.0/3.0*gsl_vector_get(k2,i)+4./9*gsl_vector_get(k3,i))*h);
  
  f(x+h,yth,k4);

  for (i = 0;i<n;i++) {
    gsl_vector_set(yt,i,gsl_vector_get(yx,i)+h*(7./24*gsl_vector_get(k1,i)+1./4*gsl_vector_get(k2,i)+1./3*gsl_vector_get(k3,i)+1./8*gsl_vector_get(k4,i)));
    gsl_vector_set(err,i,gsl_vector_get(yth,i)-gsl_vector_get(yt,i));
  } 

  //TODO GET ERRORS

}

int ode_driver(
  double* t,                             /* the current value of the variable */
  double b,                              /* the end-point of the integration */
  double* h,                             /* the current step-size */
  gsl_vector*yt,                          /* y(t) all */
  gsl_matrix*yts,
  gsl_matrix*errors,                           /* error all */
  double acc,                            /* absolute accuracy goal */
  double eps,                         /* relative accuracy goal */
  void stepper(                          /* the stepper function to be used */
    double x,                                  /* the current value of the variable */
    double h,                                  /* the step to be taken */
    gsl_vector* yx,                                /* the current value y(t) of the sought function */
    void f(double t, gsl_vector* y, gsl_vector* dydt), /* the right-hand-side, dydt = f(t,y) */
    gsl_vector* yth,                               /* output: y(t+h) */
    gsl_vector* err),
  void f(double t,gsl_vector*y,gsl_vector*dydt) /* right-hand-side */
) {

  int n = yt->size;
  double a = *t;
  double tol;
  double normErr;

  int i;
  int steps = 0; 
  int ti = 0;

  gsl_vector *yth = gsl_vector_alloc(n);
  gsl_vector *err = gsl_vector_alloc(n);
  for(i = 0; i < n;i++) {
    gsl_matrix_set(yts,i,0,gsl_vector_get(yt,i));
    gsl_matrix_set(errors,i,0,0);
  }

  while(*t<b) {
    if(*t+*h>b) *h = b-*t; 
    stepper(*t,*h,yt,f,yth,err);

    tol = ( eps*gsl_blas_dnrm2(yth) + acc ) * sqrt(*h/(b-a));
    normErr = gsl_blas_dnrm2(err);

    if( normErr < tol ) 
    {
      *t += *h;
      gsl_vector_memcpy(yt,yth);
      ti++;
      for(i = 0; i < n;i++) {
        gsl_matrix_set(yts,i,ti,gsl_vector_get(yt,i));
        gsl_matrix_set(errors,i,ti,gsl_vector_get(err,i));
      }
    }

    *h *= pow(tol/normErr,0.25)*0.95; // eq. 40
    steps ++;
  }

  gsl_vector_free(yth);
  gsl_vector_free(err);

  return steps;

}