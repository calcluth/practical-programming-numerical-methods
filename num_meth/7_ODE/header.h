#include<stdlib.h>
#include<assert.h>
#include<stdio.h>
#include<math.h>
#include<gsl/gsl_matrix.h>
#include <gsl/gsl_blas.h>
typedef struct {int size1, size2; double *data;} matrix;
typedef struct {int size; double *data;} vector;