#include"header.h"
void rkstep23(
  double x,                                  /* the current value of the variable */
  double h,                                  /* the step to be taken */
  gsl_vector* yx,                                /* the current value y(t) of the sought function */
  void f(double t, gsl_vector* y, gsl_vector* dydt), /* the right-hand-side, dydt = f(t,y) */
  gsl_vector* yth,                               /* output: y(t+h) */
  gsl_vector* err                                /* output: error estimate dy */
);

int ode_driver(
  double* t,                             /* the current value of the variable */
  double b,                              /* the end-point of the integration */
  double* h,                             /* the current step-size */
  gsl_vector*yt,                          /* y(t) all */
  gsl_matrix*yts,
  gsl_matrix*errors,                           /* error all */
  double acc,                            /* absolute accuracy goal */
  double eps,                         /* relative accuracy goal */
  void stepper(                          /* the stepper function to be used */
    double x,                                  /* the current value of the variable */
    double h,                                  /* the step to be taken */
    gsl_vector* yx,                                /* the current value y(t) of the sought function */
    void f(double t, gsl_vector* y, gsl_vector* dydt), /* the right-hand-side, dydt = f(t,y) */
    gsl_vector* yth,                               /* output: y(t+h) */
    gsl_vector* err),
  void f(double t,gsl_vector*y,gsl_vector*dydt) /* right-hand-side */
);

void harm_func(double t,gsl_vector* y,gsl_vector*dydt) {
	gsl_vector_set(dydt,0,gsl_vector_get(y,1));
	gsl_vector_set(dydt,1,-gsl_vector_get(y,0));
}

void third_order(double t,gsl_vector* y,gsl_vector*dydt) {
	//for y'''+5y''+u'+sin(x)y=0
	gsl_vector_set(dydt,0,gsl_vector_get(y,1)); //gg
	gsl_vector_set(dydt,1,gsl_vector_get(y,2));
	gsl_vector_set(dydt,2,-sin(gsl_vector_get(y,0))-gsl_vector_get(y,1)-5*gsl_vector_get(y,2));
}

int main() {

	printf("//////////////////////Harmonic Oscillator Function//////////////////////\n\n");


	int n = 2;
	int steps = 10000;
	double t = 0;
	double h = 0.01; //stepsize estimate
	double acc = 1e-3;
	double eps = 1e-6;
	
	double b = 5; //end


	gsl_vector* yt = gsl_vector_alloc(n); //y(t)
	gsl_matrix* yts = gsl_matrix_alloc(n,steps); //y(t)
	gsl_matrix* errors = gsl_matrix_alloc(n,steps); //y(t)


	gsl_vector_set(yt,0,0);
	gsl_vector_set(yt,1,-1);

	int steps_sin = ode_driver(&t,b,&h,yt,yts,errors,acc,eps,&rkstep23,harm_func);



	printf("We solve the system of ODEs equivelant to d^2y/dx^2 = -y for y_0 = 0 and y'_0 = -1\n");
	printf("y'(%g) = %g y(%g) = %g\n",t,gsl_vector_get(yt,0),t,gsl_vector_get(yt,1));
	printf("Actual solution is y'=%g y=%g\n",-sin(t), -cos(t));

	printf("Solved in %i steps\n",steps_sin );

	for(int i = 0; i < steps_sin; i++) {
		printf("%.5f (+/-%.5f) \t%.5f (+/-%.5f)\n", gsl_matrix_get(yts,0,i), gsl_matrix_get(errors,0,i), gsl_matrix_get(yts,1,i), gsl_matrix_get(errors,1,i));
	}

	printf("\n\n//////////////////////Third Order Example//////////////////////\n\n");


	gsl_vector_free(yt);
	gsl_matrix_free(yts);
	gsl_matrix_free(errors);

	n = 3;
	steps = 10000;
	t = 0;
	h = 0.01; //stepsize estimate
	acc = 1e-3;
	eps = 1e-6;
	
	b = 5; //end


	yt = gsl_vector_alloc(n); //y(t)
	yts = gsl_matrix_alloc(n,steps); //y(t)
	errors = gsl_matrix_alloc(n,steps); //y(t)

	gsl_vector_set(yt,0,0);
	gsl_vector_set(yt,1,1);
	gsl_vector_set(yt,2,0);

	steps_sin = ode_driver(&t,b,&h,yt,yts,errors,acc,eps,&rkstep23,third_order);

	printf("We solve the system of ODEs equivelant to y'''+5y''+u'+sin(x)y=0 for y_0 = 0 and y'_0 = 1 y''_0 = 0\n");
	printf("y''(%g) = %g y'(%g) = %g y(%g) = %g\n",t,gsl_vector_get(yt,0),t,gsl_vector_get(yt,1),t,gsl_vector_get(yt,2));

	printf("Solved in %i steps\n",steps_sin);

	for(int i = 0; i < steps_sin; i++) {
		printf("%.5f (+/-%.5f) \t%.5f (+/-%.5f)\t%.5f (+/-%.5f)\n", gsl_matrix_get(yts,0,i), gsl_matrix_get(errors,0,i), gsl_matrix_get(yts,1,i), gsl_matrix_get(errors,1,i), gsl_matrix_get(yts,2,i), gsl_matrix_get(errors,2,i));
	}

	gsl_vector_free(yt);
	gsl_matrix_free(yts);
	gsl_matrix_free(errors);

	return 0;
}