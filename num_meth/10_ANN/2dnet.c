#include"header.h"

double my_func(double x, double y) {
	return cos(x)+y*y/2.0;
}

double act_func(double x, double y) {
	return x*exp(-x*x)*y*exp(-y*y);
}

int main (void)
{

	//printf("/////////ANN///////////\n\n");

	int in = 2;
	int N = 15; //hidden neurons
	ann_2d* network = ann_alloc_2d(in,N,&act_func);

	double a = 0;
	double b = 5;
	double x;
	double y;
	double z;
	int points = 10;

	gsl_vector* xs = gsl_vector_alloc(points);
	gsl_vector* ys = gsl_vector_alloc(points);
	gsl_vector* zs = gsl_vector_alloc(points);

	for(int i=0;i<points;i++) {
		for(int j=0;j<points;j++) {
			x = a+(b-a)*i/(points-1);
			y = a+(b-a)*j/(points-1);
			z = my_func(x,y);
			gsl_vector_set(xs,i,x);
			gsl_vector_set(ys,i,y);
			gsl_vector_set(zs,i,z);

			printf("%lg\t%lg\t%lg\n",x,y,z);
		}
	}

	printf("\n\n");

	for(int i=0;i<N; i++) {
		gsl_vector_set(network->data,i,0.); //a_i X
		gsl_vector_set(network->data,network->n+i,1.0); //b_i X 
		gsl_vector_set(network->data,2*network->n+i,1.);	//w_i X

		gsl_vector_set(network->data,3*network->n+i,0.); //a_i Y
		gsl_vector_set(network->data,4*network->n+i,1.0); //b_i Y
		gsl_vector_set(network->data,5*network->n+i,1.0);	//w_i Y
	}
	
	ann_train_2d(network, xs, ys, zs);
	
	double neural_x, neural_y, neural_z;

	for(neural_x = a; neural_x < b; neural_x += 0.1) {
		for(neural_y = a; neural_y < b; neural_y += 0.1) {
			neural_z = ann_feed_forward_2d(network,neural_x,neural_y);

			printf("%lg\t%lg\t%lg\n",neural_x,neural_y,neural_z);
		}
	}

	ann_free_2d(network);

	return 0;
}