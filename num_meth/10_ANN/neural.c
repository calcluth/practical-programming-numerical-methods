#include"header.h"

ann* ann_alloc(int in, int number_of_hidden_neurons, double(*activation_function)(double)) {
  ann *network = (ann*)malloc(sizeof(ann));
  network->n = number_of_hidden_neurons;
  network->in = in;
  network->f = activation_function;
  network->data = gsl_vector_alloc(in*3*number_of_hidden_neurons);

  return network;
}
ann_2d* ann_alloc_2d(int in, int number_of_hidden_neurons, double(*activation_function)(double,double)) {
  ann_2d *network = (ann_2d*)malloc(sizeof(ann));
  network->n = number_of_hidden_neurons;
  network->in = in;
  network->f = activation_function;
  network->data = gsl_vector_alloc(in*3*number_of_hidden_neurons);

  return network;
}

void ann_free_2d(ann_2d* network) {
  gsl_vector_free(network->data);
  free(network);
}
void ann_free(ann* network) {
  gsl_vector_free(network->data);
  free(network);
}

double ann_feed_forward(ann* network, double x) {
  double neural_y = 0;
  for(int i =0; i < network->n; i++) {
    double a = gsl_vector_get(network->data,0*network->n+i);
    double b = gsl_vector_get(network->data,1*network->n+i);
    double w = gsl_vector_get(network->data,2*network->n+i);
    neural_y += network->f((x-a)/b)*w;
  }
  return neural_y;
}

double ann_feed_forward_2d(ann_2d* network, double x, double y) {
  double neural_z= 0;
  for(int i =0; i < network->n; i++) {
    double a_x = gsl_vector_get(network->data,0*network->n+i);
    double b_x = gsl_vector_get(network->data,1*network->n+i);
     double w_x = gsl_vector_get(network->data,2*network->n+i);

    double a_y = gsl_vector_get(network->data,3*network->n+i);
    double b_y = gsl_vector_get(network->data,4*network->n+i);
    //double w_y = gsl_vector_get(network->data,5*network->n+i);
    neural_z += network->f((x-a_x)/b_x,(y-a_y)/b_y)*w_x;
 }
  //return 0;
  return neural_z;
}

void ann_train(ann* network, gsl_vector* xs, gsl_vector* ys) {

  double cost(const gsl_vector* X, void* params) { //X is paramaters
    double c = 0;
    gsl_vector_memcpy(network->data,X); //copy our data into the point X ready for min func
    
    for(int i=0; i<xs->size;i++) { //foreach input data
      double x = gsl_vector_get(xs,i);
      double y = gsl_vector_get(ys,i);
      double neural_y = ann_feed_forward(network,x);

      c += (y-neural_y)*(y-neural_y);
    }

    return c/xs->size;
  }

  int n = 3*network->n; //since we have 3 paramaters each TIMES 3 hidden nodes
  gsl_vector* X = gsl_vector_alloc(n);
  gsl_vector_memcpy(X,network->data); //move to our current point in paramater space


  gsl_multimin_function F;
  F.f = cost;
  F.n = n;
  F.params = NULL;

  gsl_vector* step = gsl_vector_alloc(n);
  gsl_vector_set_all(step, 0.01);

  gsl_multimin_fminimizer* min = gsl_multimin_fminimizer_alloc(gsl_multimin_fminimizer_nmsimplex2,n);

  gsl_multimin_fminimizer_set(min,&F,X,step);

  int steps = 0;
  int status = 0;
  double tol = 1e-4;

  do {
    steps++;
    status = gsl_multimin_fminimizer_iterate(min);
    if(status != 0) {
      printf("Stopped - can't go further\n");
      break;
    }
    if(min->size<tol) {
      break;
    }
  } while(steps < 1000000000);

  gsl_vector_free(X);
  gsl_vector_free(step);
  gsl_multimin_fminimizer_free(min);

}
void ann_train_2d(ann_2d* network, gsl_vector* xs, gsl_vector* ys, gsl_vector* zs) {

 // printf("AT B) %lg\n", gsl_vector_get(network->data,3));

  double cost(const gsl_vector* X, void* params) { //X is paramaters
//  printf("AT C) %lg\n", gsl_vector_get(network->data,3));

    double c = 0;
    gsl_vector_memcpy(network->data,X); //copy our data into the point X ready for min func
    for(int i=0; i<xs->size;i++) { //foreach input data

      double x = gsl_vector_get(xs,i);
      double y = gsl_vector_get(ys,i);
      double z = gsl_vector_get(zs,i);
      double neural_z = ann_feed_forward_2d(network,x,y);

      c += (z-neural_z)*(z-neural_z);
    }
    return c/xs->size;
  }
  int N = 3*network->n*network->in; //since we have 3 paramaters each TIMES 3 hidden nodes
  gsl_vector* X = gsl_vector_alloc(N);
  gsl_vector_memcpy(X,network->data); //move to our current point in paramater space

  
  gsl_multimin_function F;
  F.f = cost;
  F.n = N;
  F.params = NULL;

  gsl_vector* step = gsl_vector_alloc(N);
  gsl_vector_set_all(step, 0.001);


   gsl_multimin_fminimizer* min = gsl_multimin_fminimizer_alloc(gsl_multimin_fminimizer_nmsimplex2,N);

   gsl_multimin_fminimizer_set(min,&F,X,step);

  int steps = 0;
  int status = 0;
  double tol = 1e-6;

  do {
    steps++;
    status = gsl_multimin_fminimizer_iterate(min);
    if(status != 0) {
      printf("Stopped - can't go further\n");
      break;
    }
    if(min->size<tol) {
      break;
    }
  } while(steps < 100000000000);

  gsl_vector_free(X);
  gsl_vector_free(step);
  gsl_multimin_fminimizer_free(min);

}