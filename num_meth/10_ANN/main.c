#include"header.h"

double my_func(double x) {
	return sin(3*x)*exp(-x*x);
}

double act_func(double x) {
	return x*exp(-x*x);
}

int main (void)
{

	//printf("/////////ANN///////////\n\n");

	int N = 15; //hidden neurons
	ann* network = ann_alloc(1,N,&act_func);

	double a = 0;
	double b = 5;
	double x;
	double y;
	int points = 45;

	gsl_vector* xs = gsl_vector_alloc(points);
	gsl_vector* ys = gsl_vector_alloc(points);

	for(int i=0;i<points;i++) {
		x = a+(b-a)*i/(points-1);
		y = my_func(x);
		gsl_vector_set(xs,i,x);
		gsl_vector_set(ys,i,y);

		printf("%lg\t%lg\n",x,y);
	}

	printf("\n\n");

	for(int i=0;i<N; i++) {
		gsl_vector_set(network->data,i,0); //a_i
		gsl_vector_set(network->data,network->n+i,1); //b_i
		gsl_vector_set(network->data,2*network->n+i,1);	//w_i
	}

	ann_train(network, xs, ys);

	double neural_x, neural_y;

	for(neural_x = a; neural_x < b; neural_x += 0.01) {
		neural_y = ann_feed_forward(network,neural_x);

		printf("%lg\t%lg\n",neural_x,neural_y);
	}

	return 0;
}