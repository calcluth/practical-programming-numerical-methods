#include<omp.h>
#include<time.h>
#include<stdlib.h>
#include<assert.h>
#include<stdio.h>
#include<math.h>
#include<gsl/gsl_vector.h>
#include<gsl/gsl_multimin.h>

typedef struct {int in; int n; double (*f)(double); gsl_vector* data;} ann;

typedef struct {int in; int n; double (*f)(double,double); gsl_vector* data;} ann_2d;

ann_2d* ann_alloc_2d(int in,int number_of_hidden_neurons, double(*activation_function)(double,double));
ann* ann_alloc(int in,int number_of_hidden_neurons, double(*activation_function)(double));
void ann_free(ann* network);
void ann_free_2d(ann_2d* network);
double ann_feed_forward(ann* network, double x);
void ann_train(ann* network, gsl_vector* xlist, gsl_vector* ylist);
void ann_train_2d(ann_2d* network, gsl_vector* xs, gsl_vector* ys, gsl_vector* zs);
double ann_feed_forward_2d(ann_2d* network, double x, double y);