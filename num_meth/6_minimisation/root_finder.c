#include"header.h"


matrix * matrix_alloc(int n, int m) ;
double matrix_get(matrix* A, int i, int j);
void matrix_set(matrix* A, int i, int j, double a);
void givens_qr(matrix *A);
void matrix_free(matrix* A);  
void matrix_copy(matrix* A, matrix* B);

void qr_gs_decomp(matrix* A, matrix* R);

void matrix_multiply_const(matrix* A,double con) ;
void matrix_sub(matrix* A, matrix* B);
void matrix_add_to(matrix* A, matrix* B, matrix* C);
void givens_qr_solve(matrix* QR, matrix* b);
void matrix_print(matrix* A);
double matrix_modulos(matrix* A, int i, int direction) ;
void qr_gs_solve(matrix* Q, matrix* R, matrix* b,matrix* x) ;
void matrix_transpose(matrix* A,matrix* B);
void qr_gs_inverse(matrix* Q, matrix* R, matrix* B);
void matrix_multiply(matrix* A, matrix* B, matrix* C);
double matrix_dot(matrix* A, matrix* B) ;
double matrix_outer(matrix* A, matrix* B, matrix* C);

void matrix_I(matrix* A);

void newton(
  double func_H(matrix* X, matrix* df, matrix* H),
  matrix* xstart,
  double dx,
  double epsilon
) {
    int n = xstart->size1;
    matrix* X = matrix_alloc(n,1);
    matrix* df = matrix_alloc(n,1);
    matrix* H = matrix_alloc(n,n);
    matrix* H_inv = matrix_alloc(n,n);
    matrix* R = matrix_alloc(n,n);
    matrix* Q = matrix_alloc(n,n);

    matrix_copy(xstart,X);

    double fx = func_H(X,df,H);

    // matrix* fx_i = matrix_alloc(n,1);
    // matrix* fx_f = matrix_alloc(n,1);
     matrix* Y = matrix_alloc(n,1);
     matrix* Dx = matrix_alloc(n,1);

    matrix_set(Dx,0,0,0.);
    matrix_set(Dx,1,0,0.);
   // matrix_print(Dx);
    int count = 0;
    int func_calls = 0;

    double s;
    double lambda;
    double fy;
    do{
        count++;

      qr_gs_decomp(H,R);
    // matrix_transpose(J,Q);
      qr_gs_inverse(H,R,H_inv);
      matrix_multiply(H_inv,df,Dx);
      matrix_multiply_const(Dx,-1.0);

      s = 1;
      lambda = 1e-4;

      do {
        func_calls++;
        matrix_multiply_const(Dx,s);
        matrix_add_to(Y,X,Dx);  
        // printfunc_H("x\n");m
        // matrix_print(x);
        // printfunc_H("Dx\n");
        // matrix_print(Dx);
        // printfunc_H("y\n");
        // matrix_print(y);
        fy = func_H(Y,df,H);
        matrix_multiply_const(Dx,1.0/s);
        // printf("y\n");
        // matrix_print(fy);
        // printf("now %d\n\n\n", func_calls);
        s/=2.0;

      }  while (fy>fx+lambda*s*matrix_dot(Dx,df) && s>1.0/128);


      matrix_copy(Y,X);
      fx = fy;
      // printf("mod %lg and %lg\n",matrix_modulos(fx_f,0,1),matrix_modulos(Dx,0,1));
      // printf("we got\n");
      // matrix_print(Dx);
      // printf("and got\n");
      // matrix_print(fx_f);
      } while(matrix_modulos(df,0,1)>epsilon);

      printf("Ran %d (%d) iterations\n", count,func_calls);
      //matrix_print(x);
      matrix_copy(X,xstart);


      matrix_free(Dx);
      matrix_free(X);
      matrix_free(H);
      matrix_free(df);
      matrix_free(H_inv);
      matrix_free(R);
      matrix_free(Y);
      matrix_free(Q);
}


void numerical_gradient(double func(gsl_vector* x), gsl_vector* x, gsl_vector* grad, double eps) {
    assert(grad->size == x->size);

    double fx = func(x); 
    for (int i = 0; i < x->size; ++i) {
        double x_i = gsl_vector_get(x,i);
        double dx = fabs(x_i)*eps;

        if (fabs(x_i) < sqrt(eps)) dx = eps;

        gsl_vector_set(x,i,x_i+dx);

        double fx_2 = func(x);
        gsl_vector_set(x,i,x_i);
        gsl_vector_set(grad,i,(fx_2-fx)/dx); //https://en.wikipedia.org/wiki/Numerical_differentiation
    }
}


int newton_num(double (*f)(gsl_vector* x),gsl_vector* x_start, double acc, double eps) {
    int n = x_start->size, iterations = 0;

    gsl_vector* grad = gsl_vector_alloc(n);
    gsl_vector* grad_z = gsl_vector_alloc(n);

    gsl_matrix* H0 = gsl_matrix_alloc(n,n); 

    gsl_vector* Dx = gsl_vector_alloc(n);
    gsl_vector* Dx2 = gsl_vector_alloc(n);
    gsl_vector* S = gsl_vector_alloc(n);

    gsl_vector* y = gsl_vector_alloc(n);
    gsl_vector* z = gsl_vector_alloc(n);

    numerical_gradient(f,x_start,grad,eps);
    double f_x = f(x_start);
    double f_z;

    gsl_matrix_set_identity(H0);
    do {
        iterations++;
        gsl_blas_dgemv(CblasNoTrans,-1,H0,grad,0,Dx);
        if (gsl_blas_dnrm2(Dx) < eps*gsl_blas_dnrm2(x_start) ||gsl_blas_dnrm2(grad) < acc) break;

        double s = 1;

        do {
            gsl_vector_memcpy(z,x_start);
            gsl_vector_scale(S,s);
            gsl_vector_memcpy(S,Dx);
            gsl_vector_add(z,S);
            f_z = f(z);

            double Sgrad; 
            gsl_blas_ddot(S,grad,&Sgrad);
            double alpha = 0.01;

            if (f_z < f_x+alpha*Sgrad) break;
            if (s < eps) {
                gsl_matrix_set_identity(H0);
                break;
            }

            s/=2.0;
            gsl_vector_scale(Dx,1./2);
        } while (1); //until break

        numerical_gradient(f,z,grad_z,eps);
        gsl_vector_memcpy(y,grad_z);

        gsl_blas_daxpy(-1,grad,y); // y = gradient of z - grad of y
        gsl_vector_memcpy(Dx2,Dx);
        gsl_blas_dgemv(CblasNoTrans,-1,H0,y,1,Dx2);

        double Sy;
        gsl_blas_ddot(Dx,y,&Sy);

        if (fabs(Sy) > 1e-12) { 
            gsl_blas_dger(1.0/Sy,Dx2,Dx,H0); // equation 13
        }

        gsl_vector_memcpy(x_start,z);
        gsl_vector_memcpy(grad,grad_z);
        f_x = f_z;
    } while(1); //until break

    gsl_vector_free(grad);
    gsl_vector_free(grad_z);
    gsl_matrix_free(H0);
    gsl_vector_free(Dx);
    gsl_vector_free(Dx2);
    gsl_vector_free(S);
    gsl_vector_free(y);
    gsl_vector_free(z);
    return iterations;
}


/// IGNORE - couldnt do it with my own matrix library so switched to gsl instead!!
void newton_broyden(
  double func_noH(matrix* X, matrix* df),
  matrix* xstart,
  double dx,
  double epsilon
) {
    int n = xstart->size1;
    int count = 0;
    int func_calls = 0;
    double s;
   // double lambda;
    double fy;
    double alpha = 1e-6;
   // int max = 1e6;
    double fx;
    double dfdx;
    double vector_prod;
    matrix* X = matrix_alloc(n,1);
    matrix* Dfx = matrix_alloc(n,1);
    matrix* Dx = matrix_alloc(n,1);
    matrix* df = matrix_alloc(n,1);
    matrix* S = matrix_alloc(n,1);
    matrix* Snew = matrix_alloc(n,1);
    matrix* df_y = matrix_alloc(n,1);
    matrix* SH_inv = matrix_alloc(n,1);
    matrix* H_inv_new = matrix_alloc(n,n);
    matrix* H = matrix_alloc(n,n);
    matrix* W = matrix_alloc(n,n);
    matrix* H_inv = matrix_alloc(n,n);
    matrix* R = matrix_alloc(n,n);
    matrix* Q = matrix_alloc(n,n);
    matrix* Outer = matrix_alloc(n,n);

    matrix_copy(xstart,X);

    // matrix* fx_i = matrix_alloc(n,1);
    // matrix* fx_f = matrix_alloc(n,1);
    matrix* Y = matrix_alloc(n,1);

    matrix_set(S,0,0,0.);
    matrix_set(S,1,0,0.);
   // matrix_print(Dx);

    matrix_I(H_inv);
    double norm_df_y;
    double norm_S;
    do{
      count++;
      fx = func_noH(X,df);
      printf("H_inv\n");
      matrix_print(H_inv);
      printf("df\n");
      matrix_print(df);
      printf("X\n");
      matrix_print(X);
      
    // matrix_transpose(J,Q);

      matrix_multiply(H_inv,df,S);
      printf("s\n");
      matrix_print(S);
      matrix_multiply_const(S,-1.0);

      s = 1.0; //dividor! nothing to do with S (vector)
      


      matrix_multiply_const(S,s);
      matrix_add_to(Y,X,S);
      matrix_multiply_const(S,1.0/s);
      fy = func_noH(Y,df_y); //fxpldx SEEMS GOOD

      dfdx = matrix_dot(S,df); ///GOOD TIL HERE

      norm_S = matrix_modulos(S,0,1);

      while(fy>fx+alpha*s*dfdx) {
        s /= 2;
        printf("2) is %lg < %lg ??\n", norm_S*s*s, epsilon);
        printf("fxpldx %lg\n",fy);
        if(norm_S*s*s < 1e-4) { //reset H_inv
          matrix_I(H_inv);
          break;
        }
        matrix_multiply_const(S,s);
        matrix_add_to(Y,X,S);
        matrix_multiply_const(S,1.0/s);
        //matrix_copy(Y,X);

        fy = func_noH(Y,df_y); //fxpldx SEEMS GOOD

//        matrix_copy(Y,X);

        func_calls++;


        // printf("1) is %lg < %lg ??\n", fy, fx + alpha*s*dfdx);
        // if(fy < fx + alpha*s*dfdx) break; //maybe no s!!


      }

      norm_df_y = matrix_modulos(df_y,0,1); //prob ok
      matrix_multiply_const(df_y,-1.0);
      printf("norm of dfx2 is %lg\n", norm_df_y);



      matrix_add_to(Dfx,df,df_y);
      matrix_multiply_const(df_y,-1.0); //flip sign back - maybe not neccesary?
      
      printf("df_y\n"); //dfx2
      matrix_print(df_y); //from 2nd
      printf("df\n");
      matrix_print(df);
      printf("Dfx\n");
      matrix_print(Dfx);

      printf("H_inv\n");
      matrix_print(H_inv);
      printf("df_y\n");
      matrix_print(df_y);
      matrix_multiply(H_inv,df_y,Dx);

      printf("Dx\n");
      matrix_print(Dx);
      matrix_multiply_const(Dx,-1.0);
      matrix_copy(Y,X);


      printf("norm_df_y %lg\n", norm_df_y);
    //  matrix_add_to(S,Dx,W);

printf("Dx\n");
      matrix_print(Dx); //nope not right... for some reason
printf("S\n");
      matrix_print(S); //correct

      matrix_multiply(H_inv,S,SH_inv);
printf("SH_inv\n");
      matrix_print(SH_inv);
      matrix_outer(Dx,SH_inv,Outer);
      printf("Outer\n");
      matrix_print(Outer);

      // matrix_multiply(H_inv,S,Snew);

      // matrix_copy(Snew,S);
printf("Dfx\n");
      matrix_print(Dfx); //nope

      vector_prod = -1*matrix_dot(Dfx,S);
      matrix_multiply_const(Outer,1.0/vector_prod);
printf("scale\n");
      matrix_print(Outer); //nope
      matrix_add_to(H_inv_new,Outer,H_inv); //set first element sum of these two things

      matrix_print(H_inv_new);

      matrix_copy(H_inv_new,H_inv);
      matrix_print(H_inv);
      printf("finally\n");
      // printf("mod %lg and %lg\n",matrix_modulos(fx_f,0,1),matrix_modulos(Dx,0,1));
      // printf("we got\n");
      // matrix_print(Dx);
      // printf("and got\n");
      // matrix_print(fx_f);
    //  break;

      printf(" SUC%lg\n", norm_df_y);
      //break;
    } while(norm_df_y>0.1 && count <4);

    printf("Ran %d (%d) iterations\n", count,func_calls);
    //matrix_print(x);
    matrix_copy(X,xstart);


    matrix_free(Dx);
    matrix_free(Dfx);
    matrix_free(X);
    matrix_free(H);
    matrix_free(W);
    matrix_free(df);
    matrix_free(df_y);
    matrix_free(H_inv);
    matrix_free(SH_inv);
    matrix_free(S);
    matrix_free(Snew);
    matrix_free(H_inv_new);
    matrix_free(R);
    matrix_free(Y);
    matrix_free(Q);
    matrix_free(Outer);
}
