#include"header.h"

matrix * matrix_alloc(int n, int m);
void matrix_set(matrix* A, int i, int j, double a);
double matrix_get(matrix* A, int i, int j);
void matrix_free(matrix* A);
void matrix_print(matrix* A);
void qr_gs_decomp(matrix* A, matrix* R);
void qr_gs_solve(matrix* Q, matrix* R,matrix* b,matrix* x);
void qr_gs_inverse(matrix* Q, matrix* R, matrix* B);
void matrix_multiply(matrix* A, matrix* B, matrix* C);
void matrix_copy(matrix* A, matrix* B);
void givens_qr(matrix *A);
void givens_qr_QTvec(matrix *QR, matrix* v);
void back_sub(matrix* R, matrix* x) ;
void givens_qr_solve(matrix* QR, matrix* b);
void givens_qr_unpack_Q(matrix* QR, matrix* Q);
void givens_qr_inverse(matrix* QR, matrix* B);
void givens_qr_unpack_R(matrix* QR, matrix* R);
void matrix_transpose(matrix* A,matrix* B);

double funs(int i, double x){
   switch(i){
	   case 0: return 1.0; break;
	   case 1: return x;   break;
	   case 2: return x*x;     break;
	   default: {fprintf(stderr,"funs: wrong i:%d",i); return NAN;}
   }
}

int main() {
	int n = 9;
	int m = 3; //number of functions
	double a;
	double x[n];
	double y[n];
	double dy[n];
	int i =0;
	while( scanf("%lg",&a) != EOF ){
		if(i<9) x[i] = a;
		if(i>=9 && i<18) y[i-9] = a;
		if(i>=18) dy[i-18] = a;
		i++;
	}
	// double x[] = {-2.1, -1.5, -1.03, -0.42, 0.02, 0.58, 1.06, 1.45, 2.03};
	// double y[] = {2.1, 0.73, -0.25, -0.65, 0.04, 0.85, 2.06, 3.65, 6.15};
	// double dy[]= {1.24, 0.894, 1.31, 0.898, 0.71, 0.598, 0.535, 0.768, 0.978};
	matrix* A = matrix_alloc(n,m);
	matrix* b = matrix_alloc(n,1);
	matrix* c = matrix_alloc(m,1);
	matrix* Q = matrix_alloc(n,m);
	matrix* R = matrix_alloc(m,m);
	matrix* AtAQ = matrix_alloc(m,m);
	matrix* AtAR = matrix_alloc(m,m);
	matrix* covariance = matrix_alloc(m,m);

	matrix* At = matrix_alloc(m,n);
	matrix* AtA = matrix_alloc(m,m);

	for(int i=0; i<n; i++) {
		matrix_set(b,i,0,y[i]/dy[i]);
		for(int k=0; k<m; k++) {
			matrix_set(A,i,k,funs(k,x[i])/dy[i]);			
		}
	}

	matrix_copy(A,Q); //copy to Q
	qr_gs_decomp(Q,R);
	qr_gs_solve(Q,R,b,c);

	//get transpose of A
	matrix_transpose(A,At);

	matrix_multiply(At,A,AtA);

	matrix_copy(AtA,AtAQ); //copy to Q
	qr_gs_decomp(AtAQ,AtAR);
	qr_gs_inverse(AtAQ,AtAR,covariance);

	double point, c0error,c1error,c2error,lower,upper;

	for(double j=x[0];j<=x[n-1];j+=0.02)
	{
		c0error = sqrt(matrix_get(covariance,0,0));
		c1error = sqrt(matrix_get(covariance,1,1));
		c2error = sqrt(matrix_get(covariance,2,2));

		point = matrix_get(c,0,0)*funs(0,j)+matrix_get(c,1,0)*funs(1,j)+matrix_get(c,2,0)*funs(2,j);
		lower = (matrix_get(c,0,0)-c0error)*funs(0,j)+(matrix_get(c,1,0)-c1error)*funs(1,j)+(matrix_get(c,2,0)-c2error)*funs(2,j);
		upper = (matrix_get(c,0,0)+c0error)*funs(0,j)+(matrix_get(c,1,0)+c1error)*funs(1,j)+(matrix_get(c,2,0)+c2error)*funs(2,j);

		printf("%.6f\t%.6f\t%.6f\t%.6f\n",j,point,lower,upper);
	}

	// printf("\n");

	matrix_free(A);
	matrix_free(At);
	matrix_free(AtA);
	matrix_free(AtAQ);
	matrix_free(AtAR);
	matrix_free(b);
	matrix_free(Q);
	matrix_free(R);
	matrix_free(c);
	matrix_free(covariance);

}