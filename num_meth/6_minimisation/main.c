#include"header.h"


matrix * matrix_alloc(int n, int m) ;
double matrix_get(matrix* A, int i, int j);
void matrix_set(matrix* A, int i, int j, double a);
void givens_qr(matrix *A);
void matrix_free(matrix* A);	
void matrix_print(matrix* A);
void qr_gs_decomp(matrix* A, matrix* R);
int gsl_steps_1 (void);
int gsl_steps_2 (void);
int gsl_steps_3 (void);

void newton(
  double func_H(matrix* X, matrix* df, matrix* H),
  matrix* xstart,
  double dx,
  double epsilon
) ;

void newton_broyden(
  double func_noH(matrix* X, matrix* df),
  matrix* xstart,
  double dx,
  double epsilon
);

double noH_rosenbrock(gsl_vector* X){
	double x = gsl_vector_get(X,0);
	double y = gsl_vector_get(X,1);

	// matrix_set(df,0,0,-2*(1-x)-200*(y-x*x)*2*x);
	// matrix_set(df,1,0,200*(y-x*x));

	double fx = (1-x)*(1-x)+100*(y-x*x)*(y-x*x);
	return fx;
}

double H_rosenbrock(matrix* X, matrix* df, matrix* H){
	double x = matrix_get(X,0,0);
	double y = matrix_get(X,1,0);

	matrix_set(df,0,0,-2*(1-x)-200*(y-x*x)*2*x);
	matrix_set(df,1,0,200*(y-x*x));

	matrix_set(H,0,0,2-400*(y-3*x*x));
	matrix_set(H,0,1,-400*x);
	matrix_set(H,1,0,-400*x);
	matrix_set(H,1,1,200);

	double fx = (1-x)*(1-x)+100*(y-x*x)*(y-x*x);
	return fx;
}
double H_himmelblau(matrix* X, matrix* df, matrix* H){
	double x = matrix_get(X,0,0);
	double y = matrix_get(X,1,0);

	matrix_set(df,0,0,2*(2*x*(x*x+y-11)+x+y*y-7));
	matrix_set(df,1,0,2*(x*x+2*y*(x+y*y-7)+y-11));

	matrix_set(H,0,0,4*(3*x*x+y-11)+2);
	matrix_set(H,0,1,4*x+4*y);
	matrix_set(H,1,0,4*y+4*x);
	matrix_set(H,1,1,2+4*(x+3*y*y-7));

	double fx = (x*x+y-11)*(x*x+y-11) + (x+y*y-7)*(x+y*y-7);
	return fx;
}
double noH_himmelblau(gsl_vector* X){
	double x = gsl_vector_get(X,0);
	double y = gsl_vector_get(X,1);


	double fx = (x*x+y-11)*(x*x+y-11) + (x+y*y-7)*(x+y*y-7);
	return fx;
}

int main() {

	printf("//////////////////////Rosenbrock//////////////////////");

		matrix* x_start = matrix_alloc(2,1);
		
		printf("\nFor the Rosenbrock's valley function we run the Newton's method with back-tracking linesearch and Hessian matrix...\n");
		

		matrix_set(x_start,0,0,-1);
		matrix_set(x_start,1,0,-1);

		printf("We start with the trial\n");
		matrix_print(x_start);
		newton(H_rosenbrock, x_start, 1e-6, 1e-3);
		printf("We find our minimum to be at\n");
		matrix_print(x_start);
		printf("\n");
		printf("\n");


		printf("--Now Quasi-Newton method--\n"); //works in ANN exercise I dont know why it wont work here!!!! Himblau works so probably something really minor

		gsl_vector* q_x_start = gsl_vector_alloc(2);
		gsl_vector_set(q_x_start,0,-1);
		gsl_vector_set(q_x_start,1,-1);

		printf("We start with the trial\n");
		for(int i =0; i < 2; i++) printf("\t%lg\n",gsl_vector_get(q_x_start,i));
		newton_num(noH_rosenbrock, q_x_start, 1e-6, 1e-3);
		printf("We find our minimum to be at\n");
		for(int i =0; i < 2; i++) printf("\t%lg\n",gsl_vector_get(q_x_start,i));
		printf("\n");

	printf("//////////////////////Himmelblau//////////////////////");

	x_start = matrix_alloc(2,1);
	printf("\nFor the Himmelblau's function we run the Newton's method with back-tracking linesearch and numerical Jacobian...\n");

	printf("Root 1:\n");
	matrix_set(x_start,0,0,-2.7);
	matrix_set(x_start,1,0,3.1);

	printf("We start with the trial\n");
	matrix_print(x_start);
	newton(H_himmelblau, x_start, 1e-3, 1e-3);
	printf("We find our minima to be at\n");
	matrix_print(x_start);

	printf("Root 2:\n");
	matrix_set(x_start,0,0,3);
	matrix_set(x_start,1,0,3);

	printf("We start with the trial\n");
	matrix_print(x_start);
	newton(H_himmelblau, x_start, 1e-3, 1e-3);
	printf("We find our minima to be at\n");
	matrix_print(x_start);
	printf("\n");

	printf("Root 3:\n");
	matrix_set(x_start,0,0,-4);
	matrix_set(x_start,1,0,-4);

	printf("We start with the trial\n");
	matrix_print(x_start);
	newton(H_himmelblau, x_start, 1e-3, 1e-3);
	printf("We find our minima to be at\n");
	matrix_print(x_start);
	printf("\n");

	printf("Root 4:\n");
	matrix_set(x_start,0,0,4);
	matrix_set(x_start,1,0,-2);

	printf("We start with the trial\n");
	matrix_print(x_start);
	newton(H_himmelblau, x_start, 1e-3, 1e-3);
	printf("We find our minima to be at\n");
	matrix_print(x_start);
	printf("\n");



	printf("--Now Quasi-Newton method--\n");

	gsl_vector_set(q_x_start,0,0);
	gsl_vector_set(q_x_start,1,0);

	printf("We start with the trial\n");
	for(int i =0; i < 2; i++) printf("\t%lg\n",gsl_vector_get(q_x_start,i));
	newton_num(noH_himmelblau, q_x_start, 1e-6, 1e-3);
	printf("We find our minimum to be at\n");
	for(int i =0; i < 2; i++) printf("\t%lg\n",gsl_vector_get(q_x_start,i));
	printf("\n");



	// printf("\twith analytic Jacobian\n");
	// matrix_set(x_start,0,0,0);
	// matrix_set(x_start,1,0,0);

	// printf("We start with the trial\n");
	// matrix_print(x_start);
	// newton_with_jacobian(func_3, J_3, x_start, 1e-6, 1e-3);
	// printf("We find our minimum to be at\n");
	// matrix_print(x_start);

	// printf("GSL takes %d steps\n", gsl_steps_3 ());


	gsl_vector_free(q_x_start);
	matrix_free(x_start);






	return 0;
}