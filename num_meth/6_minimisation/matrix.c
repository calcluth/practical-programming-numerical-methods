#include"header.h"


matrix * vector_alloc(int n, int m) { ; /* allocates matrix and sets size*/
  assert(n>0 && m>0);
  matrix* A = (matrix*)malloc(sizeof(matrix));
  
  A->size1 = n;
  A->size2 = m;

  A->data = (double*)malloc(n*m*sizeof(double));

  if( A==NULL ) fprintf(stderr,"error in matrix_alloc\n");
  return A;
};

matrix * matrix_alloc(int n, int m) { ; /* allocates matrix and sets size*/
  assert(n>0 && m>0);
  matrix* A = (matrix*)malloc(sizeof(matrix));
  
  A->size1 = n;
  A->size2 = m;

  A->data = (double*)malloc(n*m*sizeof(double));

  if( A==NULL ) fprintf(stderr,"error in matrix_alloc\n");
  return A;
};

void matrix_set(matrix* A, int i, int j, double a) {
 // assert(A->size1 > i && A->size2 > j);
  A->data[i+A->size1*j] = a;
}

void matrix_set_all(matrix* A, double a) {
  	for(int i = 0; i < A->size1; i++) {
	    for(int j = 0; j < A->size2; j++) {
		  matrix_set(A,i,j,a);
		}
	}
}

double matrix_get(matrix* A, int i, int j) {
  assert(A->size1 > i && A->size2 > j);
  return A->data[i+A->size1*j];
}

double matrix_dot(matrix* A, matrix* B) {
  assert(A->size1 == B->size1 && A->size2 == 1 && B->size2 == 1);
  double dot = 0;
  for(int i = 0; i < A->size1; i++) {
    dot += matrix_get(A,i,0)*matrix_get(B,i,0);
  }
  return dot;
}

double matrix_outer(matrix* A, matrix* B, matrix* C) {
  assert(A->size1 == B->size1 && A->size2 == 1 && B->size2 == 1);
  double dot = 0;
  for(int i = 0; i < A->size1; i++) {
    for(int j=0; j < B->size1; j++) {
      matrix_set(C,i,j,matrix_get(A,i,0)*matrix_get(B,j,0));
    }
  }
  return dot;
}

void matrix_I(matrix* A) {
  assert(A->size1 == A->size2);
  for(int i = 0; i < A->size1; i++) {
    for(int j = 0; j < A->size2; j++) {
      if(i==j) matrix_set(A,i,j,1);
      else matrix_set(A,i,j,0);
    }
  }
}

void matrix_multiply(matrix* A, matrix* B, matrix* C) {
  assert(A->size2 == B->size1 && C->size1 == A->size1 && C->size2 == B->size2);
  double cell;
  for(int i = 0; i < A->size1; i++) {
    for(int j = 0; j < B->size2; j++) {
      cell = 0;
      for(int k = 0; k < A->size2; k++) {
        cell += matrix_get(A,i,k)*matrix_get(B,k,j);
      }
      matrix_set(C,i,j,cell);
    }
  }
}

void matrix_multiply_const(matrix* A,double con) {
  for(int i = 0; i < A->size1; i++) {
    for(int j = 0; j < A->size2; j++) {
      matrix_set(A,i,j,matrix_get(A,i,j)*con);
    }
  }
}

void matrix_copy(matrix* A, matrix* B) {
  assert(A->size1 == B->size1 && A->size2 == B->size2);
  for(int i = 0; i < A->size1; i++) {
    for(int j = 0; j < A->size2; j++) {
      double valu = matrix_get(A,i,j);
      matrix_set(B,i,j,valu);
    }
  }
}

double matrix_modulos(matrix* A, int i, int direction) {
  double mod = 0;
  if(direction == 1) {
    for(int j = 0; j < A->size1; j++) {
      mod += matrix_get(A,j,i)*matrix_get(A,j,i); //is this right way round?
    }
    return sqrt(mod);
  } else {
    for(int j = 0; j < A->size2; j++) {
      mod += matrix_get(A,i,j)*matrix_get(A,i,j);
    }
    return sqrt(mod);
  }
}


void back_sub(matrix* R, matrix* x) {  
  for(int i = R->size2-1; i >= 0; i--) { //last row
    for(int j = R->size2-1; j >= 0; j--) {
      if(i == j) {
        matrix_set(x,j,0,matrix_get(x,j,0)/matrix_get(R,i,j));        
      } else {
        matrix_set(x,j,0,matrix_get(x,j,0)-matrix_get(x,i,0)*matrix_get(R,j,i));     
      }
    }
  }
}

void qr_gs_decomp(matrix* A, matrix* R) {
  int m = A->size2;
  int n = A->size1;
  for(int i=0; i < m; i++) { //horizontal along m direction
    double Rii = matrix_modulos(A,i,1);
    matrix_set(R, i, i, Rii);

    for(int a = 0; a < A->size1; a++) {
      matrix_set(A, a, i, matrix_get(A,a,i)/Rii);
    }

    for(int j = i + 1; j<m; j++) {
      double Rij = 0;
      for(int a = 0; a < n; a++) {
        Rij += matrix_get(A,a,i)*matrix_get(A,a,j);
      }
      matrix_set(R, i, j, Rij);
      for(int a = 0; a < A->size1; a++) {
        matrix_set(A,a,j,matrix_get(A,a,j)-matrix_get(A,a,i)*Rij);
      }
    }

  }
}

void qr_gs_solve(matrix* Q, matrix* R, matrix* b,matrix* x) {
  int n = Q->size1; //since transpose (ie 3)
  int m = R->size2; //(ie 2)
  for(int i = 0; i < m; i++) {
    double sum = 0;
    for(int j = 0; j < n; j++) {
      sum += matrix_get(Q,j,i)*matrix_get(b,j,0);
    }
    matrix_set(x,i,0,sum);
  }

  back_sub(R,x);

}

void qr_gs_inverse(matrix* Q, matrix* R, matrix* B) {
 // int n = Q->size1; //since transpose
  int m = R->size2; 

  for(int a = 0; a<Q->size2; a++) {

     for(int i = 0; i < m; i++) {
        matrix_set(B,i,a,matrix_get(Q,a,i));
     }
    //   //if(j == i) {
    //     matrix_set(B,a,i,matrix_get(Q,i,i));
    //   //}
    //   // double sum = 0;
    //   // for(int j = 0; j < n; j++) {
    //   //   sum += matrix_get(Q,j,i)*matrix_get(b,j,0);
    //   // }
    //   // matrix_set(B,i,a,sum);
    // }
    
    for(int i = m-1; i >= 0; i--) { //last row
      for(int j = m-1; j >= 0; j--) {
        if(i == j) {
          matrix_set(B,j,a,matrix_get(B,j,a)/matrix_get(R,i,j));        
        } else {
          matrix_set(B,j,a,matrix_get(B,j,a)-matrix_get(B,i,a)*matrix_get(R,j,i));     
        }
      }
    }

  }

}  

void givens_qr(matrix *A) { // A-< QR
  for(int q = 0; q < A->size2; q++) {
    for(int p = q+1; p< A->size1; p++) {
      double theta=atan2(matrix_get(A,p,q),matrix_get(A,q,q));
      for (int k=q; k < A->size2; k++) {
        double xq = matrix_get(A,q,k);
        double xp = matrix_get(A,p,k);
        matrix_set(A,q,k,xq*cos(theta)+xp*sin(theta));
        matrix_set(A,p,k,-xq*sin(theta)+xp*cos(theta));
      }
      matrix_set(A,p,q,theta);
    }
  }
}

void matrix_print(matrix* A) {
  for(int i=0; i<A->size1; i++) {
    for(int j=0; j<A->size2; j++) {
      printf("%8.3f",matrix_get(A,i,j));
    }
    printf("\n");
  }
}

void matrix_sub(matrix* A, matrix* B) {
	assert(A->size1 == B->size1 && A->size2 == B->size2);
  for(int i=0; i<A->size1; i++) {
    for(int j=0; j<A->size2; j++) {
      matrix_set(A,i,j,matrix_get(A,i,j)-matrix_get(B,i,j));
    }
  }
}

void matrix_add_to(matrix* A, matrix* B, matrix* C) {
	assert(A->size1 == B->size1 && A->size2 == B->size2);
  for(int i=0; i<A->size1; i++) {
    for(int j=0; j<A->size2; j++) {
      matrix_set(A,i,j,matrix_get(B,i,j)+matrix_get(C,i,j));
    }
  }
}

void givens_qr_QTvec(matrix *QR, matrix* v) { // v <- Q(transpose) v
  for(int q = 0; q < QR->size2; q++) {
    for(int p = q+1; p< QR->size1; p++) {
      double theta=matrix_get(QR,p,q);
      double vq = matrix_get(v,q,0);
      double vp = matrix_get(v,p,0);
      matrix_set(v,q,0,vq*cos(theta)+vp*sin(theta));
      matrix_set(v,p,0,-vq*sin(theta)+vp*cos(theta));
    }
  }
}
void givens_qr_solve(matrix* QR, matrix* b) {
   givens_qr_QTvec(QR,b);
   back_sub(QR,b);
}

void matrix_free(matrix* A) {
  free(A->data);
  free(A);
}

void givens_qr_inverse(matrix* QR, matrix* B) {
  matrix* ei = matrix_alloc(QR->size1,1);
  for(int i=0;i<QR->size1;i++) {
    matrix_free(ei);
    ei = matrix_alloc(QR->size1,1);
    matrix_set(ei,i,0,1); // basis
    givens_qr_solve(QR,ei);
  }
  matrix_free(ei);
}


void givens_qr_unpack_Q(matrix* QR, matrix* Q) {

  matrix* ei = matrix_alloc(QR->size1,1);
  for(int i=0; i<QR->size1; i++){
    matrix_free(ei);
    ei = matrix_alloc(QR->size1,1);

    for(int j=0; j<QR->size2; j++){
      matrix_set(ei,j,0,0);
    }
    matrix_set(ei,i,0,1); // create identity

    givens_qr_QTvec(QR,ei);

    for(int j=0; j<QR->size2; j++) matrix_set(Q,i,j,matrix_get(ei,j,0));
  }
  

  matrix_free(ei);
}
void matrix_transpose(matrix* A,matrix* B) {
  for(int i=0; i<A->size1; i++){
    for(int j=0; j<A->size2; j++){
      matrix_set(B,j,i,matrix_get(A,i,j));
    }
  }
}
  
void givens_qr_unpack_R(matrix* QR, matrix* R){
  assert(QR->size1==QR->size2);
  assert(R->size2==QR->size2);
  for(int c=0; c <R ->size2; c++) {
    for(int r=0;r<R->size2; r++) {
    if(r<=c) matrix_set(R,r,c,matrix_get(QR,r,c));
    else matrix_set(R,r,c,0);
    }
  }
}