/////////Integrate 1/pi^3 [1-cos(x)cos(y)(cos(z)]^-1 between [0,pi] for all (Single Core)///////////

Found 1.3909968192265858011 (actual = 1.3932039296856768591842462603255) with error 0.00126061 in 10.0444 secs

/////////Integrate 1/pi^3 [1-cos(x)cos(y)(cos(z)]^-1 between [0,pi] for all (Multi Core)////////////

We are running on 4 threads
Found 1.3901502565195846817 (actual = 1.3932039296856768591842462603255) in 14.9197 secs
Slows down! Generation of random numbers is too simple of a task. The time cost of splitting the threads is greater than that which we can gain from having multiple random generators on different threads.
