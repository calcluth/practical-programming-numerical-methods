#include<omp.h>
#include<time.h>
#include<stdlib.h>
#include<assert.h>
#include<stdio.h>
#include<math.h>
void randomx(int d, double* a, double* b, double* x, unsigned int* seed);

double plainmc(double f(double* x), int dim, double* a, double* b, int N, double* error, unsigned int* seed);
double plainmc_omp(double f(double* x), int dim, double* a, double* b, int N, double* error, unsigned int* seeds);