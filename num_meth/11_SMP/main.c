#include"header.h"

int main (void)
{

	printf("/////////Integrate 1/pi^3 [1-cos(x)cos(y)(cos(z)]^-1 between [0,pi] for all (Single Core)///////////\n\n");
	clock_t tic = clock();

	int dim = 3;
	int N = 100000000;
	double error;
	double a[3] = {0, 0, 0};
	double b[3] = {M_PI, M_PI, M_PI};

	double my_func(double *x) 
	{
		return 1/M_PI/M_PI/M_PI/(1-cos(x[0])*cos(x[1])*cos(x[2]));
	}

	unsigned int single_seed = time(0)+1000;

	double result = plainmc(&my_func, dim, a, b, N, &error,&single_seed);

    clock_t toc = clock();

	printf("Found %.20lg (actual = 1.3932039296856768591842462603255) with error %lg in %lg secs\n", result,error,(double)(toc - tic) / CLOCKS_PER_SEC);

	printf("\n/////////Integrate 1/pi^3 [1-cos(x)cos(y)(cos(z)]^-1 between [0,pi] for all (Multi Core)////////////\n\n");


	tic = clock();

	int threads = omp_get_max_threads();


	unsigned int *seeds = malloc(threads * sizeof(int));
	double *results = malloc(threads * sizeof(double));
	double *errors = malloc(threads * sizeof(double));

	for(int seed = 0; seed < threads; seed++){
		seeds[seed] = time(0)-12933*seed;
	}

	N /= threads;

	#pragma omp parallel shared(seeds, results)
	{
		int thread = omp_get_thread_num();
		//printf("We are running on %d threads\n", thread);
		unsigned int seed = seeds[thread];
		results[thread] = plainmc(&my_func, dim, a, b, N, &error,&seed);
		errors[thread] = error;
	}


	printf("We are running on %d threads\n", threads);


	double mean = 0;

	for(int i =0; i<threads; i++) {
		mean += results[i];
	}		
	mean /= threads;

	toc = clock();

	printf("Found %.20lg (actual = 1.3932039296856768591842462603255) in %lg secs\n", mean,(double)(toc - tic) / CLOCKS_PER_SEC);

	printf("Slows down! Generation of random numbers is too simple of a task. The time cost of splitting the threads is greater than that which we can gain from having multiple random generators on different threads.\n");

	free(results);
	free(seeds);
	free(errors);

	return 0;
}