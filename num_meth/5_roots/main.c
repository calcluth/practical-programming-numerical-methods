#include"header.h"


matrix * matrix_alloc(int n, int m) ;
double matrix_get(matrix* A, int i, int j);
void matrix_set(matrix* A, int i, int j, double a);
void givens_qr(matrix *A);
void matrix_free(matrix* A);	
void matrix_print(matrix* A);
void qr_gs_decomp(matrix* A, matrix* R);
int gsl_steps_1 (void);
int gsl_steps_2 (void);
int gsl_steps_3 (void);

void newton(
	void f(matrix* x, matrix* fx),
	matrix* xstart,
	double dx,
	double epsilon
);

void newton_with_jacobian(
  void f(matrix* X, matrix* fx),
  void J(matrix* X, matrix* J),
  matrix* xstart,
  double dx,
  double epsilon
);

void func_1(matrix* X, matrix* fx) {
	assert(X->size1 == 2 && fx->size1 == 2);
	double A = 10000;
	double x=matrix_get(X,0,0);
	double y=matrix_get(X,1,0);

	//y is just element 0,0 of A!
	matrix_set(fx,0,0,A*x*y-1);
	matrix_set(fx,1,0,exp(-x)+exp(-y)-1-1/A);
}
void J_1(matrix* X, matrix* J){
	int A = 10000;
	double x=matrix_get(X,0,0);
	double y=matrix_get(X,1,0);
	matrix_set(J,0,0,A*y);
	matrix_set(J,0,1,A*x);
	matrix_set(J,1,0,-exp(-x));
	matrix_set(J,1,1,-exp(-y));
}

void func_2(matrix* X, matrix* fx) {
	assert(X->size1 == 2 && fx->size1 == 2);

	double x = matrix_get(X,0,0);
	double y = matrix_get(X,1,0);

	//y is just element 0,0 of A!
	matrix_set(fx,0,0, -2.0*(1-x)-400.0*x*(y-x*x));
	matrix_set(fx,1,0, 200.0*(y-x*x));
}

void J_2(matrix* X, matrix* J){
	double x = matrix_get(X,0,0);
	double y = matrix_get(X,1,0);
	matrix_set(J,0,0,2-400*(y-3*x*x));
	matrix_set(J,0,1,-400*x);
	matrix_set(J,1,0,-400*x);
	matrix_set(J,1,1,200);
}
void func_3(matrix* X, matrix* fx) {
	assert(X->size1 == 2 && fx->size1 == 2);

	double x = matrix_get(X,0,0);
	double y = matrix_get(X,1,0);

	//y is just element 0,0 of A!
	matrix_set(fx,0,0, 4*x*(x*x+y-11)+2*(x+y*y-7));
	matrix_set(fx,1,0, 2*(x*x+y-11)+4*y*(x+y*y-7));
}

void J_3(matrix* X, matrix* J){
	double x = matrix_get(X,0,0);
	double y = matrix_get(X,1,0);
	matrix_set(J,0,0,4*(3*x*x+y-11)+2);
	matrix_set(J,0,1,4*x+4*y);
	matrix_set(J,1,0,4*x+4*y);
	matrix_set(J,1,1,2+4*(x+3*y*y-7));
}

int main() {

	printf("//////////////////////System of equations//////////////////////\n");

	//////////////////sys of equations///////////////////

	matrix* x_start = matrix_alloc(2,1);
	matrix* fx_1 = matrix_alloc(2,1);
	//matrix* J_1 = matrix_alloc(2,2);



	printf("\nFor the system of equations we run the Newton's method with back-tracking linesearch...\n");
	printf("\twith numerical Jacobian\n");
	matrix_set(x_start,0,0,1);
	matrix_set(x_start,1,0,0);


	printf("We start with the trial\n");
	matrix_print(x_start);
	newton(func_1, x_start, 1e-6, 1e-3);
	printf("We find our roots to be\n");
	matrix_print(x_start);
	printf("\n\n");


	printf("\twith analytic Jacobian\n");
	matrix_set(x_start,0,0,1);
	matrix_set(x_start,1,0,0);

	printf("We start with the trial\n");
	matrix_print(x_start);
	newton_with_jacobian(func_1, J_1, x_start, 1e-6, 1e-3);
	printf("We find our roots to be\n");
	matrix_print(x_start);

	printf("GSL takes %d steps\n", gsl_steps_1 ());

	matrix_free(x_start);
	printf("\n");

	printf("//////////////////////Rosenbrock//////////////////////\n");

	//////////////////////rosenbrock//////////////////////

	x_start = matrix_alloc(2,1);
	matrix* fx_2 = matrix_alloc(1,1);

	printf("\nFor the Rosenbrock's valley function we run the Newton's method with back-tracking linesearch and numerical Jacobian...\n");
	printf("\twith numerical Jacobian\n");

	matrix_set(x_start,0,0,-1);
	matrix_set(x_start,1,0,-1);

	printf("We start with the trial\n");
	matrix_print(x_start);
	newton(func_2, x_start, 1e-6, 1e-3);
	printf("We find our minimum to be at\n");
	matrix_print(x_start);
	printf("\n");

	printf("\twith analytic Jacobian\n");
	matrix_set(x_start,0,0,-1);
	matrix_set(x_start,1,0,-1);

	printf("We start with the trial\n");
	matrix_print(x_start);
	newton_with_jacobian(func_2, J_2, x_start, 1e-6, 1e-3);
	printf("We find our minimum to be at\n");
	matrix_print(x_start);

	printf("GSL takes %d steps\n", gsl_steps_2 ());


	matrix_free(x_start);
	printf("\n");

	printf("//////////////////////Himmelblau//////////////////////\n");


	//////////////////////himmelblau//////////////////////

	x_start = matrix_alloc(2,1);
	matrix* fx_3 = matrix_alloc(1,1);
	printf("\nFor the Himmelblau's function we run the Newton's method with back-tracking linesearch and numerical Jacobian...\n");
	printf("\twith numerical Jacobian\n");

	printf("Root 1\n");
	matrix_set(x_start,0,0,3);
	matrix_set(x_start,1,0,3);

	printf("We start with the trial\n");
	matrix_print(x_start);
	newton(func_3, x_start, 1e-6, 1e-3);
	printf("We find our minimum to be at\n");
	matrix_print(x_start);
	printf("\n");

	printf("\twith analytic Jacobian\n");
	matrix_set(x_start,0,0,3);
	matrix_set(x_start,1,0,3);

	printf("We start with the trial\n");
	matrix_print(x_start);
	newton_with_jacobian(func_3, J_3, x_start, 1e-6, 1e-3);
	printf("We find our minimum to be at\n");
	matrix_print(x_start);


	printf("Root 2\n");
	matrix_set(x_start,0,0,-2.7);
	matrix_set(x_start,1,0,3.1);

	printf("We start with the trial\n");
	matrix_print(x_start);
	newton(func_3, x_start, 1e-6, 1e-3);
	printf("We find our minimum to be at\n");
	matrix_print(x_start);
	printf("\n");

	printf("\twith analytic Jacobian\n");
	matrix_set(x_start,0,0,-2.7);
	matrix_set(x_start,1,0,3.1);

	printf("We start with the trial\n");
	matrix_print(x_start);
	newton_with_jacobian(func_3, J_3, x_start, 1e-6, 1e-3);
	printf("We find our minimum to be at\n");
	matrix_print(x_start);


	printf("Root 3\n");
	matrix_set(x_start,0,0,-4);
	matrix_set(x_start,1,0,-4);

	printf("We start with the trial\n");
	matrix_print(x_start);
	newton(func_3, x_start, 1e-6, 1e-3);
	printf("We find our minimum to be at\n");
	matrix_print(x_start);
	printf("\n");

	printf("\twith analytic Jacobian\n");
	matrix_set(x_start,0,0,-4);
	matrix_set(x_start,1,0,-4);

	printf("We start with the trial\n");
	matrix_print(x_start);
	newton_with_jacobian(func_3, J_3, x_start, 1e-6, 1e-3);
	printf("We find our minimum to be at\n");
	matrix_print(x_start);


	printf("Root 4\n");
	matrix_set(x_start,0,0,4);
	matrix_set(x_start,1,0,-2);

	printf("We start with the trial\n");
	matrix_print(x_start);
	newton(func_3, x_start, 1e-6, 1e-3);
	printf("We find our minimum to be at\n");
	matrix_print(x_start);
	printf("\n");

	printf("\twith analytic Jacobian\n");
	matrix_set(x_start,0,0,4);
	matrix_set(x_start,1,0,-2);

	printf("We start with the trial\n");
	matrix_print(x_start);
	newton_with_jacobian(func_3, J_3, x_start, 1e-6, 1e-3);
	printf("We find our minimum to be at\n");
	matrix_print(x_start);

	printf("GSL takes %d steps for one root\n", gsl_steps_3 ());



	matrix_free(x_start);
	matrix_free(fx_1);
	matrix_free(fx_2);
	matrix_free(fx_3);

	return 0;
}