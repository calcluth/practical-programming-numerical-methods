#include"header.h"


matrix * matrix_alloc(int n, int m) ;
double matrix_get(matrix* A, int i, int j);
void matrix_set(matrix* A, int i, int j, double a);
void givens_qr(matrix *A);
void matrix_free(matrix* A);  
void matrix_copy(matrix* A, matrix* B);

void qr_gs_decomp(matrix* A, matrix* R);

void matrix_multiply_const(matrix* A,double con) ;
void matrix_sub(matrix* A, matrix* B);
void matrix_add_to(matrix* A, matrix* B, matrix* C);
void givens_qr_solve(matrix* QR, matrix* b);
void matrix_print(matrix* A);
double matrix_modulos(matrix* A, int i, int direction) ;
void qr_gs_solve(matrix* Q, matrix* R, matrix* b,matrix* x) ;
void matrix_transpose(matrix* A,matrix* B);

void newton_with_jacobian(
  void f(matrix* X, matrix* fx),
  void Jac(matrix* X, matrix* J),
  matrix* xstart,
  double dx,
  double epsilon
) {
    int n  = xstart->size1;
    //double df;
    matrix* x = matrix_alloc(n,1);
    matrix_copy(xstart,x);

    matrix* fx_i = matrix_alloc(n,1);
    matrix* fx_f = matrix_alloc(n,1);
    matrix* y = matrix_alloc(n,1);
    matrix* fy = matrix_alloc(n,1);
    matrix* Dx = matrix_alloc(n,1);

    matrix* J = matrix_alloc(n,n);
    matrix* R = matrix_alloc(n,n);
    matrix* Q = matrix_alloc(n,n);
    matrix_set(Dx,0,0,0.);
    matrix_set(Dx,1,0,0.);
   // matrix_print(Dx);
    int count = 0;
      int func_calls = 0;

    double s;
    do{
        count++;
      f(x,fx_i);
       // printf("x\n");
       // matrix_print(x);
       Jac(x,J);
       // printf("J\n");
    //   matrix_print(J);
       // printf("\n");
      qr_gs_decomp(J,R);
     // matrix_transpose(J,Q);
      qr_gs_solve(J,R,fx_i,Dx);
       // printf("Dx\n");
       // matrix_print(Dx);
       // matrix_print(J);
       // printf("\n");
      matrix_multiply_const(Dx,-1.0);
      s = 2;
      do {
        func_calls++;
        matrix_multiply_const(Dx,s);
        matrix_add_to(y,x,Dx);  
        // printf("x\n");
        // matrix_print(x);
        // printf("Dx\n");
        // matrix_print(Dx);
        // printf("y\n");
        // matrix_print(y);
        f(y,fy);
        matrix_multiply_const(Dx,1.0/s);
        // printf("y\n");
        // matrix_print(fy);
        // printf("now %d\n\n\n", func_calls);
        s/=2.0;

      }  while (matrix_modulos(fy,0,1)>(1-s/2)*matrix_modulos(fx_i,0,1) && s>1.0/128);


      matrix_copy(y,x);
      matrix_copy(fy,fx_f);
      // printf("mod %lg and %lg\n",matrix_modulos(fx_f,0,1),matrix_modulos(Dx,0,1));
      // printf("we got\n");
      // matrix_print(Dx);
      // printf("and got\n");
      // matrix_print(fx_f);
    } while(matrix_modulos(Dx,0,1)>dx && matrix_modulos(fx_f,0,1)>epsilon);

    printf("Ran %d (%d) iterations\n", count,func_calls);
    //matrix_print(x);
    matrix_copy(x,xstart);


    matrix_free(Dx);
    matrix_free(x);
    matrix_free(fx_i);
    matrix_free(fx_f);
    matrix_free(J);
    matrix_free(R);
    matrix_free(y);
    matrix_free(fy);
    matrix_free(Q);
}

void newton(
    void f(matrix* x, matrix* fx),
    matrix* xstart,
    double dx,
    double epsilon
  ) {
    int n  = xstart->size1;
    //double df;
    matrix* x = matrix_alloc(n,1);
    matrix_copy(xstart,x);

    matrix* fx_i = matrix_alloc(n,1);
    matrix* fx_f = matrix_alloc(n,1);
    matrix* y = matrix_alloc(n,1);
    matrix* fy = matrix_alloc(n,1);
    matrix* Dx = matrix_alloc(n,1);

    matrix* J = matrix_alloc(n,n);
    matrix* R = matrix_alloc(n,n);
    matrix* Q = matrix_alloc(n,n);
    int count = 0;

      int func_calls = 0;
    do{
      count++;
      f(x,fx_i);
      func_calls++;

      for(int j=0; j < n; j++) {
        matrix_set(x,j,0,matrix_get(x,j,0)+dx);
        f(x,fx_f);
        func_calls++;
        // printf("out to \n");
        // matrix_print(fx_f);
        // printf("1");

        matrix_sub(fx_f,fx_i);

        for(int i=0; i<n; i++) {
          matrix_set(J,i,j,matrix_get(fx_f,i,0)/dx);
        }
        matrix_set(x,j,0,matrix_get(x,j,0)-dx);
      }
      //matrix_print(J);

      qr_gs_decomp(J,R);
      //matrix_transpose(J,Q);
      qr_gs_solve(J,R,fx_i,Dx);
      matrix_multiply_const(Dx,-1.0);

      double s = 2;
      do {
        func_calls++;
        s/=2;
       // matrix_multiply_const(Dx,s);
        matrix_add_to(y,x,Dx);
        f(y,fy);        
        func_calls++;
       // matrix_multiply_const(fx_i,1.0/s);
      }  while (matrix_modulos(fx_i,0,1)>(1-s/2)*matrix_modulos(fy,0,1) && s>1.0/128);

      matrix_copy(y,x);
      matrix_copy(fy,fx_f);
    } while(matrix_modulos(Dx,0,1)>dx && matrix_modulos(fy,0,1)>epsilon);

    printf("Ran %d (%d) iterations\n", count,func_calls);
    //matrix_print(x);
    matrix_copy(x,xstart);


    matrix_free(x);
    matrix_free(fx_i);
    matrix_free(fx_f);
    matrix_free(J);
    matrix_free(R);
    matrix_free(y);
    matrix_free(fy);
    matrix_free(Q);
}