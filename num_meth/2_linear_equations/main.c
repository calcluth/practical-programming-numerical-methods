#include"header.h"

matrix * matrix_alloc(int n, int m);
void matrix_set(matrix* A, int i, int j, double a);
double matrix_get(matrix* A, int i, int j);
void matrix_free(matrix* A);
void matrix_print(matrix* A);
void qr_gs_decomp(matrix* A, matrix* R);
void qr_gs_solve(matrix* Q, matrix* R,matrix* b,matrix* x);
void qr_gs_inverse(matrix* Q, matrix* R, matrix* B);
void matrix_multiply(matrix* A, matrix* B, matrix* C);
void matrix_copy(matrix* A, matrix* B);
void givens_qr(matrix *A);
void givens_qr_QTvec(matrix *QR, matrix* v);
void back_sub(matrix* R, matrix* x) ;
void givens_qr_solve(matrix* QR, matrix* b);
void givens_qr_unpack_Q(matrix* QR, matrix* Q);
void givens_qr_inverse(matrix* QR, matrix* B);
void givens_qr_unpack_R(matrix* QR, matrix* R);
void matrix_transpose(matrix* A,matrix* B);

int main() {
	printf("\nPART A: Modified Gram-Schmidt orthogonalization\n-------\n");
	int m = 2;
	int n = 3;
	matrix* A = matrix_alloc(n,m);
	matrix* Q = matrix_alloc(n,m);
	matrix* R = matrix_alloc(m,m);
	matrix* B = matrix_alloc(n,m);
	matrix_set(A,0,0,1);
	matrix_set(A,0,1,2);
	matrix_set(A,1,0,1);
	matrix_set(A,1,1,1);
	matrix_set(A,2,0,1);
	matrix_set(A,2,1,1);

	matrix* b = matrix_alloc(n,1);
	matrix* b_out = matrix_alloc(n,1);
	matrix* x = matrix_alloc(m,1);

	matrix_set(b,0,0,3);
	matrix_set(b,1,0,1);
	matrix_set(b,2,0,1);
	matrix_set(x,0,0,0);
	matrix_set(x,1,0,0);

	printf("A=QR:\n");
	matrix_print(A);
	printf("\n");
	printf("With GR Decomposition ");


	matrix_copy(A,Q); //copy to Q

	qr_gs_decomp(Q,R);

	printf("Q:\n");
	matrix_print(Q);
	printf("\n");
	printf("R:\n");
	matrix_print(R);
	printf("\n");
	printf("We have a vector b:\n");
	matrix_print(b);

	qr_gs_solve(Q,R,b,x);
	//gr_gs_inverse(A,R,B);

	printf("\n");
	printf("Gives x:\n");
	matrix_print(x);
	printf("\n");

	printf("Check Ax=b:\n");

	matrix_multiply(A,x,b_out);
	matrix_print(b_out);


	matrix_free(A);
	matrix_free(Q);
	matrix_free(R);
	matrix_free(B);
	matrix_free(b);
	matrix_free(b_out);
	matrix_free(x);

	// PART B

	printf("\nPART B: Matrix inverse by Gram-Schmidt QR factorization\n-------\n");

	m = 4;
	n = 4;
	A = matrix_alloc(n,m);
	Q = matrix_alloc(n,m);
	R = matrix_alloc(m,m);
	B = matrix_alloc(n,m);
	matrix* C = matrix_alloc(m,n);

	matrix_set(A,0,0,1);
	matrix_set(A,0,1,2);
	matrix_set(A,0,2,1);
	matrix_set(A,0,3,5);
	matrix_set(A,1,0,3);
	matrix_set(A,1,1,5);
	matrix_set(A,1,2,5);
	matrix_set(A,1,3,3);
	matrix_set(A,2,0,1);
	matrix_set(A,2,1,2);
	matrix_set(A,2,2,2);
	matrix_set(A,2,3,2);
	matrix_set(A,3,0,3);
	matrix_set(A,3,1,4);
	matrix_set(A,3,2,6);
	matrix_set(A,3,3,3);

	printf("A:\n");
	matrix_print(A);
	matrix_copy(A,Q);
	printf("\n");

	printf("With GR Decomposition ");
	qr_gs_decomp(Q,R);

	printf("Q:\n");
	matrix_print(Q);
	printf("\n");

	qr_gs_inverse(Q,R,B);
	printf("B=A^-1:\n");
	matrix_print(B);
	printf("\n");

	matrix_multiply(A,B,C);
	printf("A*B=I:\n");
	matrix_print(C);
	printf("\n");


	matrix_free(A);
	matrix_free(R);
	matrix_free(B);
	matrix_free(C);
	matrix_free(b);
	matrix_free(x);



	printf("\nPART C: QR-decomposition by Givens rotations\n-------\n");

	m = 4;
	n = 4;
	A = matrix_alloc(n,m);
	matrix* QR = matrix_alloc(n,m);
	R = matrix_alloc(m,m);
	B = matrix_alloc(n,m);
	b = matrix_alloc(n,1);
	x = matrix_alloc(n,1);
	C = matrix_alloc(m,n);
	matrix* CQ = matrix_alloc(m,m);

	matrix_set(A,0,0,1);
	matrix_set(A,0,1,2);
	matrix_set(A,0,2,1);
	matrix_set(A,0,3,5);
	matrix_set(A,1,0,3);
	matrix_set(A,1,1,5);
	matrix_set(A,1,2,5);
	matrix_set(A,1,3,3);
	matrix_set(A,2,0,1);
	matrix_set(A,2,1,2);
	matrix_set(A,2,2,2);
	matrix_set(A,2,3,2);
	matrix_set(A,3,0,3);
	matrix_set(A,3,1,4);
	matrix_set(A,3,2,6);
	matrix_set(A,3,3,3);


	matrix_set(b,0,0,3);
	matrix_set(b,1,0,1);
	matrix_set(b,2,0,1);
	matrix_set(b,3,0,1);
	printf("With Givens Rotation QR Decomposition ");

	printf("b:\n");
	matrix_print(b);

	printf("A:\n");
	matrix_print(A);
	matrix_copy(A,QR);
	printf("\n");

	givens_qr(QR);
	printf("Givens QR (compacted): \n");
	matrix_print(QR);
	printf("\nUnpack R =  \n");

	givens_qr_unpack_R(QR,R);
	matrix_print(R);

	printf("\nUnpack Q: \n");
	givens_qr_unpack_Q(QR,Q);
	matrix_print(Q);

	printf("\nFind Transpose of Q:\n");

	matrix_transpose(Q,C);
	matrix_print(C);

	printf("\nCheck Tran(Q)Q = I:\n");

	matrix_multiply(C,Q,CQ);
	matrix_print(CQ);

	printf("\nCheck QR = A:\n");

	matrix_multiply(Q,R,QR);
	matrix_print(QR);

	printf("\nSolve for x:\n");

	matrix_multiply(C,b,x);
	back_sub(R,x);
	matrix_print(x);



	matrix_free(A);
	matrix_free(QR);
	matrix_free(B);
	matrix_free(R);
	matrix_free(C);
	matrix_free(CQ);
	matrix_free(b);
	matrix_free(x);
}