#include"header.h"

struct cspline * cspline_alloc(int n, double *x, double *y); /* allocates and builds the quadratic spline */
double cspline_evaluate(struct cspline *q, double z);        /* evaluates the prebuilt spline at point z */
double cspline_derivative(struct cspline *q, double z); /* evaluates the derivative of the prebuilt spline at point z */
double cspline_integral(struct cspline *q, double z);  /* evaluates the integral of the prebuilt spline from x[0] to z */
void cspline_free(struct cspline *q); /* free memory allocated in cspline_alloc */

int main() {

	#define N 6


    double x[N] = {};
    double y[N] = {};


    int i=0;
    while(scanf(" %lg %lg", &x[i], &y[i]) == 2){i++;};	
	// double x[] = {1.0,3.0,5.3,9,10,18.0};
	// double y[] = {12.0,17.0,12.0,4.0,3.0,6.1};

    struct cspline *q = cspline_alloc(N, x, y);
	
	for(double i =1; i<10; i+=0.1) {
		//printf("%lg\t%lg\t%lg,\t%lg\n", i, cspline_evaluate(q,i), cspline_integral(q,i), cspline_derivative(q,i));
		printf("%lg\t%lg\t%lg\t%lg\n", i, cspline_evaluate(q,i), cspline_integral(q,i), cspline_derivative(q,i));
	}

	cspline_free(q);

	return 0;
}