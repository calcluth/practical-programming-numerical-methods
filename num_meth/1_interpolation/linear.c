#include<assert.h>
#include<stdio.h>

double linterp(int n, double *x, double *y, double z) {
	assert(n > 1 && z >= x[0] && z <= x[n-1]);


	// int div = 2;
	// int i = n/div;

	// while(n/div != 0) {
	// 	if(x[i]>z) {
	// 		i-=n/div;
	// 	} else {
	// 		i+=n/div;
	// 	}
	// 	printf("i:%i div:%i xi: %lg \n",i,div,x[i]);
	// 	div = div*2;
	// }


	int i =0; //at beginning of array
	int j = n-1; //at end of array
	while(j-i>1) { //while dif is more than 1, ie otherwise we have found the point they are the same
		int m = (i+j)/2; //avg of two points = centre of them
		if(z > x[m]) { //if our value is more than the val at x[m]
			i = m; // then we move our left pointer to this middle value
		} else {
			j = m; //otherwise we move our right poitner to this midle value
		}
	}

	return y[i]+(y[i+1]-y[i])/(x[i+1]-x[i])*(z-x[i]);
}

double linterp_integ(int n, double *x, double *y, double z) {
	assert(n > 1 && z >= x[0] && z <= x[n-1]);

	double integral = 0;
	int i = 0;
	double at_z  = linterp(n,x,y,z);

	while(x[i+1] < z) {
		integral += (y[i+1]+y[i])/2*(x[i+1]-x[i]);
		i++;
	}
	integral += (at_z+y[i])/2*(z-x[i]);


	/*
	while(i < n && x[i] <= z) {
		start = x[i];
		if(x[i+1] > z) { end = z;} else { end = x[i+1]; }
		printf("start %lg end %lg p1 %lg, p2 %lg, p3 %lg",start,end,(y[i+1]-y[i])/(x[i+1]-x[i])/2,(end*end-start*start),y[i]*(end-start));
		integral += (y[i+1]-y[i])/(x[i+1]-x[i])/2*(end*end-start*start)+y[i]*(end-start);
		printf("int %lg",integral);
		i++;
	} */
	return integral;
}

int main() {
	#define N 6


    double x[N] = {};
    double y[N] = {};
    int i=0;
    while(scanf(" %lg %lg", &x[i], &y[i]) == 2){i++;};	
	// double x[] = {1.0,3.0,5.3,9,10,18.0};
	// double y[] = {12.0,17.0,12.0,4.0,3.0,6.1};
	
	for(double i =1; i<10; i+=0.1) {
		printf("%lg\t%lg\t%lg\n", i, linterp(N,x,y,i), linterp_integ(N,x,y,i));
	}
	return 0;
}