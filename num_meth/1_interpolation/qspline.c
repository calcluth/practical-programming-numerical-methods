#include"header.h"

qspline * qspline_alloc(int n, double *x, double *y) { ; /* allocates and builds the quadratic spline */
  qspline* q = (qspline*)malloc(sizeof(qspline));
  

  if( n<2 ) fprintf(stderr,"Requires more than 2 values in struct\n");

  q->x = (double*)malloc(n*sizeof(double));
  q->y = (double*)malloc(n*sizeof(double));
  q->b = (double*)malloc((n-1)*sizeof(double));
  q->c = (double*)malloc((n-1)*sizeof(double));
  q->n = n;

  double dx[n-1];
  double dy[n-1];

  int i;

  for(i=0;i<n;i++) {
  	q->x[i] = x[i];
  	q->y[i] = y[i];
  }

  double p[n-1];

  for(i=0; i<n-1; i++) {
  	dx[i] = (x[i+1]-x[i]);
  	dy[i] = (y[i+1]-y[i]);
  }
  for(i=0; i<n-1; i++) {
  	p[i] = dy[i]/dx[i];
  }

  q->c[0] = 0;
  for(i=0; i<n-2; i++) {
  	q->c[i+1] = 1/dx[i+1]*(p[i+1]-p[i]-q->c[i]*dx[i]);

  }

  q->c[n-2] /= 2;
  for(i=n-3; i>=0; i--) {
  	q->c[i] = 1/dx[i]*(p[i+1]-p[i]-q->c[i+1]*dx[i+1]);
  }

  for(i=0;i<n-1;i++) {
  	q->b[i] = p[i] - q->c[i]*dx[i];
  }


  //(*v).data = malloc(n*sizeof(double));
  if( q==NULL ) fprintf(stderr,"error in qspline_alloc\n");
  return q;
};
double qspline_evaluate(qspline *q, double z){        /* evaluates the prebuilt spline at point z */
	assert(z >= q->x[0] && z<= q->x[q->n-1]);

	int i =0; //at beginning of array
	int j = q->n-1; //at end of array
	while(j-i>1) { //while dif is more than 1, ie otherwise we have found the point they are the same
		int m = (i+j)/2; //avg of two points = centre of them
		if(z > q->x[m]) { //if our value is more than the val at x[m]
			i = m; // then we move our left pointer to this middle value
		} else {
			j = m; //otherwise we move our right poitner to this midle value
		}
	}

	double dx = z-q->x[i];

	return q->y[i] + (q->b[i]  + q->c[i] * dx) * dx;
}
double qspline_derivative_point(qspline *q, int i, double z) {
		return q->y[i]*z+q->b[i]*z*(z/2-q->x[i])+z*z*(q->c[i]*z/3-q->x[i])+q->x[i]*q->x[i]*z;
}
double qspline_derivative(qspline *q, double z){  /* evaluates the integral of the prebuilt spline from x[0] to z */
	assert(q->n > 1 && z >= q->x[0] && z <= q->x[q->n-1]);

	int i =0; //at beginning of array
	int j = q->n-1; //at end of array
	while(j-i>1) { //while dif is more than 1, ie otherwise we have found the point they are the same
		int m = (i+j)/2; //avg of two points = centre of them
		if(z > q->x[m]) { //if our value is more than the val at x[m]
			i = m; // then we move our left pointer to this middle value
		} else {
			j = m; //otherwise we move our right poitner to this midle value
		}
	}

	//integral +=  qspline_integral_point(q, i, z)-qspline_integral_point(q, i, q->x[i]);
	return q->b[i]+2*z*q->c[i]-2*q->x[i]*q->c[i];
}
double qspline_integral_point(qspline *q, int i, double z) {
		//return q->y[i]*z+q->b[i]*z*(z/2-q->x[i])+z*z*(q->c[i]*z/3-q->x[i])+q->x[i]*q->x[i]*z;
	return q->y[i]*z + q->b[i]*z*z/2 - q->x[i]*q->b[i]*z + q->c[i]*z*z*z/3+ q->c[i]*q->x[i]*q->x[i]*z-q->x[i]*z*z*q->c[i];
}
double qspline_integral(qspline *q, double z){  /* evaluates the integral of the prebuilt spline from x[0] to z */
	assert(q->n > 1 && z >= q->x[0] && z <= q->x[q->n-1]);

	double integral = 0;
	int i = 0;
	//double at_z  =	qspline_evaluate(q,z);

	while(q->x[i+1] < z) {
		integral += qspline_integral_point(q, i, q->x[i+1])-qspline_integral_point(q, i, q->x[i]);
		i++;
	}
	integral +=  qspline_integral_point(q, i, z)-qspline_integral_point(q, i, q->x[i]);
	return integral;
}
void qspline_free(qspline *q) { /* free memory allocated in qspline_alloc */
	free(q->x);
	free(q->y);
	free(q->c);
	free(q->b);
	free(q);
} 