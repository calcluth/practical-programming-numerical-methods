#include"header.h"
#include<gsl/gsl_spline.h>
#include <gsl/gsl_interp.h>

int main() {

	#define N 6

    double x[N] = {};
    double y[N] = {};


    int i=0;

	gsl_interp_accel *acc = gsl_interp_accel_alloc ();
    gsl_spline *spline = gsl_spline_alloc (gsl_interp_cspline, N);

    while(scanf(" %lg %lg", &x[i], &y[i]) == 2){
         	i++;
    };

    gsl_spline_init (spline, x, y, N);
	
	
	double yi;
	for(double xi =x[0]; xi<x[N-1]; xi+=0.01) {
		printf("%lg\t%lg\n", xi, yi);
		yi = gsl_spline_eval(spline, xi, acc);
		//printf("%lg\t%lg\t%lg,\t%lg\n", i, cspline_evaluate(q,i), cspline_integral(q,i), cspline_derivative(q,i));
		printf("%lg\t%lg\n", xi, yi);
	}

	gsl_spline_free (spline);
    gsl_interp_accel_free (acc);
	return 0;
}