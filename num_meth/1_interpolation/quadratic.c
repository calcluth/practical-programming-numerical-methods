#include"header.h"

struct qspline * qspline_alloc(int n, double *x, double *y); /* allocates and builds the quadratic spline */
double qspline_evaluate(struct qspline *s, double z);        /* evaluates the prebuilt spline at point z */
double qspline_derivative(struct qspline *s, double z); /* evaluates the derivative of the prebuilt spline at point z */
double qspline_integral(struct qspline *s, double z);  /* evaluates the integral of the prebuilt spline from x[0] to z */
void qspline_free(struct qspline *s); /* free memory allocated in qspline_alloc */

int main() {

	#define N 6


    double x[N] = {};
    double y[N] = {};


    int i=0;
    while(scanf(" %lg %lg", &x[i], &y[i]) == 2){i++;};	
	// double x[] = {1.0,3.0,5.3,9,10,18.0};
	// double y[] = {12.0,17.0,12.0,4.0,3.0,6.1};

    struct qspline *q = qspline_alloc(N, x, y);
	
	for(double i =1; i<10; i+=0.1) {
		printf("%lg\t%lg\t%lg,\t%lg\n", i, qspline_evaluate(q,i), qspline_integral(q,i), qspline_derivative(q,i));
	}
	qspline_free(q);

	return 0;
}