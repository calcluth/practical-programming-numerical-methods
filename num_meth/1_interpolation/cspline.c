#include"header.h"

cspline * cspline_alloc(int n, double *x, double *y) { ; /* allocates and builds the quadratic spline */
  cspline* q = (cspline*)malloc(sizeof(cspline));
  

  if( n<2 ) fprintf(stderr,"Requires more than 2 values in struct\n");

  q->x = (double*)malloc(n*sizeof(double));
  q->y = (double*)malloc(n*sizeof(double));
  q->b = (double*)malloc(n*sizeof(double));
  q->d = (double*)malloc((n-1)*sizeof(double));
  q->c = (double*)malloc((n-1)*sizeof(double));
  q->n = n;

  double dx[n-1];
  double dy[n-1];

  int i;

  for(i=0;i<n;i++) {
  	q->x[i] = x[i];
  	q->y[i] = y[i];
  }

  double p[n-1];

  for(i=0; i<n-1; i++) {
  	dx[i] = (x[i+1]-x[i]);
  	dy[i] = (y[i+1]-y[i]);
  	assert(dx[i] > 0);
  }
  for(i=0; i<n-1; i++) {
  	p[i] = dy[i]/dx[i];
  }

  double D[n], Q[n-1], B[n];

  D[0] = 2;

  for(int i = 0; i<n-2;i++) {
  	D[i+1] = 2*dx[i]/dx[i+1]+2;
  }

  D[n-1] = 2;

  Q[0] = 1;

  for(int i=0; i<n-2;i++) {
  	Q[i+1]=dx[i]/dx[i+1];
  }

  for(int i=0;i<n-2;i++) {
  	B[i+1] = 3*(p[i]+p[i+1]*dx[i]/dx[i+1]);
  }

  B[0] = 3*p[0];
  B[n-1] = 3*p[n-1];

  for(int i=1;i<n;i++) {
  	D[i]-=Q[i-1]/D[i-1];
  	B[i]-=B[i-1]/D[i-1];
  }

  q->b[n-1] = B[n-1]/D[n-1];

  for(int i=n-2;i>=0;i--) {
  	q->b[i]=(B[i]-Q[i]*q->b[i+1])/D[i];
  }

  for(int i=0;i<n-1;i++) {
  	q->c[i]=(-2*q->b[i]-q->b[i+1]+3*p[i])/dx[i];
  	q->d[i]=(q->b[i]+q->b[i+1]-2*p[i])/dx[i]/dx[i];
  }

  //(*v).data = malloc(n*sizeof(double));
  if( q==NULL ) fprintf(stderr,"error in cspline_alloc\n");
  return q;
};
double cspline_evaluate(cspline *q, double z){        /* evaluates the prebuilt spline at point z */
	assert(z >= q->x[0] && z <= q->x[q->n-1]);

	int i =0; //at beginning of array
	int j = q->n-1; //at end of array
	while(j-i>1) { //while dif is more than 1, ie otherwise we have found the point they are the same
		int m = (i+j)/2; //avg of two points = centre of them
		if(z > q->x[m]) { //if our value is more than the val at x[m]
			i = m; // then we move our left pointer to this middle value
		} else {
			j = m; //otherwise we move our right poitner to this midle value
		}
	}

	double dx = z-q->x[i];

	return q->y[i] + dx*(q->b[i] + dx*(q->c[i] + dx*q->d[i]));
}
double cspline_derivative_point(cspline *q, int i, double z) {
		return q->y[i]*z+q->b[i]*z*(z/2-q->x[i])+z*z*(q->c[i]*z/3-q->x[i])+q->x[i]*q->x[i]*z;
}
double cspline_derivative(cspline *q, double z){  /* evaluates the integral of the prebuilt spline from x[0] to z */
	assert(q->n > 1 && z >= q->x[0] && z <= q->x[q->n-1]);
	//double at_z  =	cspline_evaluate(q,z);

	int i =0; //at beginning of array
	int j = q->n-1; //at end of array
	while(j-i>1) { //while dif is more than 1, ie otherwise we have found the point they are the same
		int m = (i+j)/2; //avg of two points = centre of them
		if(z > q->x[m]) { //if our value is more than the val at x[m]
			i = m; // then we move our left pointer to this middle value
		} else {
			j = m; //otherwise we move our right poitner to this midle value
		}
	}
	//integral +=  cspline_integral_point(q, i, z)-cspline_integral_point(q, i, q->x[i]);
	return q->b[i]+2*q->c[i]*z-2*q->x[i]*q->c[i]+3*q->d[i]*z*z-6*q->d[i]*q->x[i]*z+3*q->d[i]*q->x[i]*q->x[i];
}
double cspline_integral_point(cspline *q, int i, double z) {
		//return q->y[i]*z+q->b[i]*z*(z/2-q->x[i])+z*z*(q->c[i]*z/3-q->x[i])+q->x[i]*q->x[i]*z;
	return q->y[i]*z+q->b[i]*z*z/2-q->b[i]*q->x[i]*z+q->c[i]*z*z*z/3+q->c[i]*q->x[i]*q->x[i]*z-q->c[i]*q->x[i]*z*z+q->d[i]*z*z*z*z/4-q->d[i]*q->x[i]*z*z*z-q->x[i]*q->x[i]*q->x[i]*q->d[i]*z+1.5*q->d[i]*q->x[i]*q->x[i]*z*z;
}
double cspline_integral(cspline *q, double z){  /* evaluates the integral of the prebuilt spline from x[0] to z */
	assert(q->n > 1 && z >= q->x[0] && z <= q->x[q->n-1]);

	double integral = 0;
	int i = 0;
	//double at_z  =	cspline_evaluate(q,z);

	while(q->x[i+1] < z) {
		integral += cspline_integral_point(q, i, q->x[i+1])-cspline_integral_point(q, i, q->x[i]);
		i++;
	}
	integral +=  cspline_integral_point(q, i, z)-cspline_integral_point(q, i, q->x[i]);
	return integral;
}
void cspline_free(cspline *q) { /* free memory allocated in cspline_alloc */
	free(q->x);
	free(q->y);
	free(q->c);
	free(q->b);
	free(q->d);
	free(q);
} 