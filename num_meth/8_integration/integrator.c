#include"header.h"
double adapt24(double f(double),
  double a,
  double b,
  double acc,
  double eps,
  double f2,
  double f3,
  double* err,
  int recursions) {
    assert(recursions < 1000000);
    double f1 = f(a+(b-a)/6);
    double f4 = f(a+(b-a)*5.0/6);
    double Q = (2*f1+f2+f3+2*f4)*(b-a)/6;
    double q = (f1+f2+f3+f4)*(b-a)/4;
    double tol = acc+eps*fabs(Q);
    *err = fabs(Q-q);
    if(*err<tol) return Q;
    else {
      double err_1;
      double err_2;
      double new_middle = (a+b)/2;
      double new_acc = acc/sqrt(2.0);
      double Q1 = adapt24(f,a,new_middle,new_acc,eps,f1,f2,&err_1,recursions+1);
      double Q2 = adapt24(f,new_middle,b,new_acc,eps,f3,f4,&err_2,recursions+1);
      *err = sqrt( pow(err_1,2) + pow(err_2,2) );
      return Q1+Q2;
    }
}

double adaptive_integrator(double f(double),
  double a, //start
  double b, //end
  double acc, //relative error
  double eps, //abs error
  double *err) //estimate of error
{
  if(isinf(a) && isinf(b)) {
    double inf_inf_func(double t) {
      return f(t/(1-t*t)) * (1+t*t)/(1-t*t)/(1-t*t);
    }    
    double inf_a = -1, inf_b = 1;

    double f2 = inf_inf_func(inf_a+2.0*(inf_b-inf_a)/6);
    double f3 = inf_inf_func(inf_a+4.0*(inf_b-inf_a)/6);
    int recursions = 0;
    return adapt24(inf_inf_func,inf_a,inf_b,acc,eps,f2,f3,err,recursions);
  }
  if(isinf(b)) {
    //return adaptiveIntegratorOpen(pmInf,aNew,bNew,abs,eps,err);

    double a_inf_func(double t) {
      return f(a+(1-t)/t)/pow(t,2);
    }    
    double inf_a = 0, inf_b = 1;

    double f2 = a_inf_func(inf_a+2.0*(inf_b-inf_a)/6);
    double f3 = a_inf_func(inf_a+4.0*(inf_b-inf_a)/6);
    int recursions = 0;
    return adapt24(a_inf_func,inf_a,inf_b,acc,eps,f2,f3,err,recursions);
  }
  if(isinf(a)) {
    //return adaptiveIntegratorOpen(pmInf,aNew,bNew,abs,eps,err);

    double a_inf_func(double t) {
      return f(b-(1-t)/t)/t/t;
    }    
    double inf_a = 0, inf_b = 1;

    double f2 = a_inf_func(inf_a+2.0*(inf_b-inf_a)/6);
    double f3 = a_inf_func(inf_a+4.0*(inf_b-inf_a)/6);
    int recursions = 0;
    return adapt24(a_inf_func,inf_a,inf_b,acc,eps,f2,f3,err,recursions);
  }
  // if(a == -INFINITY && b != INFINITY) {
  //   double inf_a = 0, inf_b =1;
  //   double inf_b_func(double t) {
  //     return(f(a-(1.-t)/t)/t/t);
  //   }    
  //   double f2 = inf_b_func(a+2.0*(b-a)/6);
  //   double f3 = inf_b_func(a+4.0*(b-a)/6);
  //   int recursions = 0;
  //   return adapt24(inf_b_func,inf_a,inf_b,acc,eps,f2,f3,err,recursions);
  // }
  double f2 = f(a+2.0*(b-a)/6);
  double f3 = f(a+4.0*(b-a)/6);
  int recursions = 0;
  return adapt24(f,a,b,acc,eps,f2,f3,err,recursions);
}

