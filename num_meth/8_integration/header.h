#include<stdlib.h>
#include<assert.h>
#include<stdio.h>
#include<math.h>
#include<gsl/gsl_matrix.h>
#include <gsl/gsl_blas.h>
double adapt24(double f(double),
  double a,
  double b,
  double acc,
  double eps,
  double f2,
  double f3,
  double* err,
  int recursions);

double adaptive_integrator(double f(double),
  double a, //start
  double b, //end
  double acc, //relative error
  double eps, //abs error
  double *err); //estimate of error