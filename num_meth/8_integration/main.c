#include"header.h"
double my_function(double x){return x*x;}
double dmitri_1(double x){return sqrt(x);}
double dmitri_2(double x){return 1./sqrt(x);}
double dmitri_3(double x){return log(x)/sqrt(x);}

int main (void)
{

	printf("//////////////////////Integrate x^2 between 0 and 3//////////////////////\n\n");

	double a=0, b=3, err, acc=0.0001, eps=0.0001;
	double Q = adaptive_integrator(my_function, a, b, acc, eps, &err);
	printf("integral=%lg, actual = 9, error=%lg\n",Q,err);

	printf("\n\n//////////////////////Integrate 1/sqrt(x) between 0 and 1//////////////////////\n\n");

	a=0, b=1;
	Q = adaptive_integrator(dmitri_1, a, b, acc, eps, &err);
	printf("integral=%lg, actual = 2/3,error=%lg\n",Q,err);

	printf("\n\n//////////////////////Integrate 1/ln(x) between 0 and 1//////////////////////\n\n");

	a=0, b=1;
	Q = adaptive_integrator(dmitri_2, a, b, acc, eps, &err);
	printf("integral=%lg, actual = 2, error=%lg\n",Q,err);

	printf("\n\n//////////////////////Integrate ln(x)/sqrt(x) between 0 and 1//////////////////////\n\n");

	a=0, b=1;
	Q = adaptive_integrator(dmitri_3, a, b, acc, eps, &err);
	printf("integral=%lg, actual = -4, error=%lg\n",Q,err);

	printf("\n\n//////////////////////Integrate to get pi (between 0 and 1)//////////////////////\n\n");

	int fd, fe_2, fe_3, fe_4 = 0;
	double dmitri_4(double x){ //put function inside so we can calulate number of calculations!
		fd ++;
		return 4.*sqrt(1.-(1.-x)*(1.-x));
	}
	double my_function_2(double x){
		fe_2++;
		return x*exp(-x*x);
	}
	double my_function_3(double x){
		fe_3++;
		return exp(-x*x);
	}
	double my_function_4(double x){
		fe_4++;
		return exp(-x*x/2);
	}

	a=0, b=1, acc = 1e-3, eps = 1e-6; //put back to 20!!!!! 
	Q = adaptive_integrator(dmitri_4, a, b, acc, eps, &err);
	printf("calculated PI=%.30lg with %d calculations, actual = pi, error=%lg\n",Q,fd,err);
	printf("Real PI = 3.1415926535897932384626433832795028841971693993751058209749445923078164062862089986280348253421170679821480865132823066470938446095505822317253594081284");
	
	printf("\n\n//////////////////////Integrate xe^{-x^2} (between 0 and and infinity)//////////////////////\n\n");

	a=0, b=INFINITY, acc = 1e-3, eps = 1e-6;
	Q = adaptive_integrator(my_function_2, a, b, acc, eps, &err);
	printf("integral=%.10lg with %d calculations, actual = 1/2, error=%lg\n",Q,fe_2,err);
	
	printf("\n\n//////////////////////Integrate e^{-x^2} (between -infinity and and infinity)//////////////////////\n\n");

	a=-INFINITY, b=INFINITY, acc = 1e-3, eps = 1e-6;
	Q = adaptive_integrator(my_function_3, a, b, acc, eps, &err);
	printf("integral=%.10lg with %d calculations, actual = sqrt(pi)=1.77245385091, error=%lg\n",Q,fe_3,err);

	printf("\n\n//////////////////////Integrate e^{-x^2/2} (between -infinity and and 0)//////////////////////\n\n");

	a=-INFINITY, b=0, acc = 1e-3, eps = 1e-6;
	Q = adaptive_integrator(my_function_4, a, b, acc, eps, &err);
	printf("integral=%.10lg with %d calculations, actual = sqrt(pi/2)=1.25331413732, error=%lg\n",Q,fe_4,err);
	return 0;
}