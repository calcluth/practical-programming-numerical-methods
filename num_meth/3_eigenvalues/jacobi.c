#include"header.h"

int create_diagonal(gsl_vector *b, gsl_matrix* B) {
  assert(b->size == B->size1 && b->size == B->size2);
  for(int i = 0; i < b->size; i++) {
    gsl_matrix_set(B,i,i,gsl_vector_get(b,i));
  }
  return 0;
}

int generate_symmetric_matrix(gsl_matrix* A, int n, int max) {
  for(int i = 0; i<n; i++) {
    for(int j = i; j <n; j++) {
      srand(n+5+i+j);
      int r = rand()%(max*2)-max;
      gsl_matrix_set(A,i,j,r);
      gsl_matrix_set(A,j,i,r);
    }
  }
  return 0;
}

int print_matrix(gsl_matrix *A) {
  int i, j;
  for (i = 0; i < A->size1; i++) {
    for (j = 0; j < A->size2; j++)
      printf("%lg\t", gsl_matrix_get(A,i,j));
    printf("\n");
  }
  printf("\n");
  return 0;
}

int print_vector(gsl_vector *b) {
  for(int i=0; i<b->size; i++) {
    printf("%lg\t", gsl_vector_get(b,i));
  }
  printf("\n\n");
  return 0;
}

int jacobi_rotation(gsl_matrix* A, gsl_vector* b, gsl_matrix* V, int p, int q, int sort_desc) {
  double App = gsl_vector_get(b, p);
  double Aqq = gsl_vector_get(b, q);
  double Apq = gsl_matrix_get(A, p, q);
  double phi;

  if(sort_desc == 1) phi = -0.5*atan2(2*Apq,-Aqq+App);
  if(sort_desc == 0) phi = 0.5*atan2(2*Apq,Aqq-App);

  double c = cos(phi);
  double s = sin(phi);
  int rows = A->size1;

  double App_new = c*c*App-2*s*c*Apq+s*s*Aqq;
  double Aqq_new = s*s*App+2*s*c*Apq+c*c*Aqq;

  int complete = 0;

  if(App_new == App && Aqq_new == Aqq) {
    complete = 1;
  } else {
    gsl_vector_set(b,p,App_new);
    gsl_vector_set(b,q,Aqq_new);
    gsl_matrix_set(A,p,q,0);
    for(int i=0; i<p; i++) {
      double Aip = gsl_matrix_get(A,i,p);
      double Aiq = gsl_matrix_get(A,i,q);
      gsl_matrix_set(A,i,p,c*Aip-s*Aiq);
      gsl_matrix_set(A,i,q,c*Aiq+s*Aip);
    }
    for(int i=p+1;i<q; i++) {
      double Api = gsl_matrix_get(A,p,i);
      double Aiq = gsl_matrix_get(A,i,q);
      gsl_matrix_set(A,p,i,c*Api-s*Aiq);
      gsl_matrix_set(A,i,q,c*Aiq+s*Api);
    }
    for(int i=q+1;i<rows; i++) {
      double Api = gsl_matrix_get(A,p,i);
      double Aqi = gsl_matrix_get(A,q,i);
      gsl_matrix_set(A,p,i,c*Api-s*Aqi);
      gsl_matrix_set(A,q,i,c*Aqi+s*Api);
    }
    for(int i=0;i<rows; i++) {
      double Vip = gsl_matrix_get(V,i,p);
      double Viq = gsl_matrix_get(V,i,q);
      gsl_matrix_set(V,i,p,c*Vip-s*Viq);
      gsl_matrix_set(V,i,q,c*Viq+s*Vip);
    }
  }
  return complete;

}
int jacobi(gsl_matrix* A, gsl_vector* b, gsl_matrix* V) {
  int complete = 0;
  int iterations = 0;
  int rows = A->size1;

  for(int row = 0; row < rows; row ++)
    gsl_vector_set(b, row, gsl_matrix_get(A,row,row));

  gsl_matrix_set_identity(V);

  do{
    iterations++;
    int p, q;
    for(p=0;p<rows;p++) {
      for(q=p+1;q<rows;q++) {
         complete = jacobi_rotation(A,b,V,p,q,0);
      }
    }
  }while(complete==0);


  return iterations;
}

int jacobi_eigbyeig(gsl_matrix* A, gsl_vector* b, gsl_matrix* V, int sort_desc, int max_eig) {
  int complete = 0;
  int iterations = 0;
  int rows = A->size1;

  for(int row = 0; row < rows; row ++)
    gsl_vector_set(b, row, gsl_matrix_get(A,row,row));

  gsl_matrix_set_identity(V);

  int p, q;
  for(p=0;p<max_eig && p <rows;p++) {
    do{
      iterations++;
      for(q=p+1;q<rows;q++) {
         complete = jacobi_rotation(A,b,V,p,q,sort_desc);
      }
    }while(complete==0);
  }


  return iterations;
}

int get_max_of_row(gsl_matrix* A, int p) {
  int q;
  double Apq;

  int max_q = 0;
  double maxApq = gsl_matrix_get(A,p,0);

  for(q=1;q<A->size1;q++) {
    Apq = gsl_matrix_get(A,p,q);
    if(Apq > maxApq) {
      maxApq = Apq;
      max_q = q;
    }
  }
  return max_q;
}

int jacobi_classic(gsl_matrix* A, gsl_vector* b, gsl_matrix* V) {
  int complete = 0;
  int iterations = 0;
  int rows = A->size1;

  for(int row = 0; row < rows; row ++)
    gsl_vector_set(b, row, gsl_matrix_get(A,row,row));

  gsl_matrix_set_identity(V);

  int p;
  int max_q;

  do{
    iterations++;
    for(p=0;p<rows;p++) {
      max_q = get_max_of_row(A,p);
      complete = jacobi_rotation(A,b,V,p,max_q,0);
    }
  }while(complete==0);


  return iterations;
}