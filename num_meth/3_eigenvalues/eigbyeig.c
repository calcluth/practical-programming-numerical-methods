#include"header.h"

int jacobi_eigbyeig(gsl_matrix* A, gsl_vector* b, gsl_matrix* V, int sort_desc, int max_eig);
int print_matrix(gsl_matrix *A) ;
int print_vector(gsl_vector *b);

int create_diagonal(gsl_vector *b, gsl_matrix* B);
int generate_symmetric_matrix(gsl_matrix* A, int n, int max);

int main(int argc, char *argv[]) {
	assert(argc > 1);
	int n = atoi(argv[1]);

	int sort_desc = 0;
	int max_eig = n;

	if(argc > 2 && atoi(argv[2]) == 1) sort_desc = 1;
	if(argc > 3 && atoi(argv[3]) < n && atoi(argv[3]) > 0) max_eig = atoi(argv[3]);

	gsl_matrix * A = gsl_matrix_alloc (n, n);
	gsl_matrix * V = gsl_matrix_alloc (n, n);
	gsl_matrix * C = gsl_matrix_alloc (n, n);
	gsl_matrix * B = gsl_matrix_alloc (n, n);
	gsl_vector * b = gsl_vector_alloc (n);
	srand(2);   // should only be called once

	generate_symmetric_matrix(A,n,10);

	printf("Matrix:\n");
	print_matrix(A);

	int iterations = jacobi_eigbyeig(A,b,V,sort_desc,max_eig);

	printf("Eigenvalues:\n");
	print_vector(b);

	printf("Eigenvectors:\n");
	print_matrix(V);

	printf("A = V^T b V:\n");
	create_diagonal(b,B);
	gsl_blas_dgemm (CblasNoTrans, CblasTrans, 1.0, B, V, 0.0, C);
	gsl_blas_dgemm (CblasNoTrans, CblasNoTrans, 1.0, V, C, 0.0, A);
	print_matrix(B);

	printf("Solved in %i sweeps.\n", iterations);

	printf("This method orders the eigenvalues since we do the iterations row by row. This process is essentially a minimisation whereby it can affect other (non-diagonal) rows. So we first minimise row 1, then row 2 etc.\n");

	gsl_matrix_free(A);
	gsl_matrix_free(V);
	gsl_matrix_free(C);
	gsl_vector_free(b);
	return 0;
}