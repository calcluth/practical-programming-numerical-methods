#include"header.h"

int jacobi(gsl_matrix* A, gsl_vector* b, gsl_matrix* V);
int print_matrix(gsl_matrix *A) ;
int print_vector(gsl_vector *b);

int create_diagonal(gsl_vector *b, gsl_matrix* B);
int generate_symmetric_matrix(gsl_matrix* A, int n, int max);

int main(int argc, char *argv[]) {
	assert(argc > 1);
	int n = atoi(argv[1]);

	gsl_matrix * A = gsl_matrix_alloc (n, n);
	gsl_matrix * V = gsl_matrix_alloc (n, n);
	gsl_matrix * C = gsl_matrix_alloc (n, n);
	gsl_matrix * B = gsl_matrix_alloc (n, n);
	gsl_vector * b = gsl_vector_alloc (n);
	srand(1);   // should only be called once

	generate_symmetric_matrix(A,n,10);
	// gsl_matrix_set(A,0,0,1);
	// gsl_matrix_set(A,0,1,2);
	// gsl_matrix_set(A,0,2,3);
	// gsl_matrix_set(A,1,0,2);
	// gsl_matrix_set(A,1,1,5);
	// gsl_matrix_set(A,1,2,2);
	// gsl_matrix_set(A,2,0,3);
	// gsl_matrix_set(A,2,1,2);
	// gsl_matrix_set(A,2,2,1);

	printf("Matrix:\n");
	print_matrix(A);


	int iterations = jacobi(A,b,V);

	printf("Eigenvalues:\n");
	print_vector(b);

	printf("Eigenvectors:\n");
	print_matrix(V);

	printf("A = V^T b V:\n");
	create_diagonal(b,B);
	gsl_blas_dgemm (CblasNoTrans, CblasTrans, 1.0, B, V, 0.0, C);
	gsl_blas_dgemm (CblasNoTrans, CblasNoTrans, 1.0, V, C, 0.0, A);
	print_matrix(B);

	printf("Solved in %i sweeps.\n", iterations);

	gsl_matrix_free(A);
	gsl_matrix_free(V);
	gsl_matrix_free(C);
	gsl_vector_free(b);
	return 0;
}