#include"header.h"
#define RAND ((double)rand()/RAND_MAX)

void randomx(int d, double* a, double* b, double* x) {
  for(int i=0;i<d;i++) {
    x[i] = a[i] + RAND*(b[i]-a[i]);
  }
}

double plainmc(double f(double* x), int dim, double* a, double* b, int N, double* error){
   //var randomx = function(a,b) [a[i]+Math.random()*(b[i]-a[i]) for (i in a)];
  double volume=1;
  int i;
  double sum = 0;
  double sum2 = 0;
  double fx, x[dim];

  for(i=0;i<dim;i++) volume *= b[i]-a[i];

  for(i=0;i<N;i++){
    randomx(dim,a,b,x); //put into x

    fx=f(x);
    sum+=fx;
    sum2+=fx*fx;
  }

  double mean = sum/N;                             // <f_i>
  double sigma = sqrt(sum2/N - mean*mean);    // sigma² = <(f_i)²> - <f_i>²
  double SIGMA = sigma/sqrt(N);               // SIGMA² = <Q²> - <Q>²

  *error = SIGMA*volume;
  return mean*volume;
}