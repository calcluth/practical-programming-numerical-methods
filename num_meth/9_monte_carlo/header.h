#include<stdlib.h>
#include<assert.h>
#include<stdio.h>
#include<math.h>
#include<gsl/gsl_matrix.h>
#include <gsl/gsl_blas.h>
void randomx(int d, double* a, double* b, double* x);

double plainmc(double f(double* x), int dim, double* a, double* b, int N, double* error);