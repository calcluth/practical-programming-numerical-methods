#include"header.h"

int main (void)
{
	int dim = 3;
	double error;
	double a[3] = {2, 1, 0};
	double b[3] = {3, 2, 1};

	double func1(double *x) 
	{
		return 8*x[0]*x[0]+x[1]*x[0]+x[2]*x[1];
	}
	double first_error = 0;

	int start = 100;

	for(int N = start; N < 100000; N*=1.2){
		plainmc(&func1, dim, a, b, N, &error);
		if(first_error ==0) first_error = error;
		printf("%d\t%lg\t%lg\n", N,error,1./sqrt(N*1.)*sqrt(start)*first_error);
	}


	return 0;
}