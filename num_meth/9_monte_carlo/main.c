#include"header.h"

int main (void)
{

	printf("//////////////////////Integrate 8xyz between [2,3], [1,2], [0,1]//////////////////////\n\n");

	int dim = 3;
	int N = 100000;
	double error;
	double a[3] = {2, 1, 0};
	double b[3] = {3, 2, 1};

	double func1(double *x) 
	{
		return 8*x[0]*x[1]*x[2];
	}

	double result = plainmc(&func1, dim, a, b, N, &error);

	printf("Found %lg (actual = 15) with error %lg\n", result,error);


	printf("\n\n//////////////////////Integrate x^2+y^2+z^2 between [0,1] for all//////////////////////\n\n");

	dim = 3;
	N = 1000000;
	a[0] = 0; a[1] = 0; a[2] = 0;
	b[0] = 1; b[1] = 1; b[2] = 1;

	double func2(double *x) 
	{
		return x[0]*x[0]+x[1]*x[1]+x[2]*x[2];
	}

	result = plainmc(&func2, dim, a, b, N, &error);

	printf("Found %lg (actual = 1) with error %lg\n", result,error);

	printf("\n\n//////////////////////Integrate 1/pi^3 [1-cos(x)cos(y)(cos(z)]^-1 between [0,pi] for all//////////////////////\n\n");

	dim = 3;
	N = 100000;
	a[0] = 0; a[1] = 0; a[2] = 0;
	b[0] = M_PI; b[1] = M_PI; b[2] = M_PI;

	double func3(double *x) 
	{
		return 1/M_PI/M_PI/M_PI/(1-cos(x[0])*cos(x[1])*cos(x[2]));
	}

	result = plainmc(&func3, dim, a, b, N, &error);

	printf("Found %lg (actual = 1.3932039296856768591842462603255) with error %lg\n", result,error);
	return 0;
}