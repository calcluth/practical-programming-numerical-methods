#include<omp.h>
#include<time.h>
#include<stdlib.h>
#include<assert.h>
#include<stdio.h>
#include<math.h>
#include<gsl/gsl_vector.h>
#include<gsl/gsl_multimin.h>
#include<gsl/gsl_matrix.h>
#include<gsl/gsl_blas.h>

typedef struct {int n; double (*f)(double); double (*df)(double); gsl_vector* data; double F; double dF;} ann;

ann* ann_alloc(int number_of_hidden_neurons, double(*activation_function)(double), double(*activation_function_derivative)(double));
void ann_free(ann* network);
void ann_feed_forward(ann* network, double x);
void ann_train(ann* network, gsl_vector* xs, double(*ode)(double,double), double x0, double y0);
void numerical_gradient(double func(gsl_vector* x), gsl_vector* x, gsl_vector* grad, double eps);
int newton_num(double (*f)(gsl_vector* x),gsl_vector* x_start, double acc, double eps);