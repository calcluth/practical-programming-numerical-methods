#include"header.h"

void numerical_gradient(double func(gsl_vector* x), gsl_vector* x, gsl_vector* grad, double eps) {
    assert(grad->size == x->size);

    double fx = func(x); 
    for (int i = 0; i < x->size; ++i) {
        double x_i = gsl_vector_get(x,i);
        double dx = fabs(x_i)*eps;

        if (fabs(x_i) < sqrt(eps)) dx = eps;

        gsl_vector_set(x,i,x_i+dx);

        double fx_2 = func(x);
        gsl_vector_set(x,i,x_i);
        gsl_vector_set(grad,i,(fx_2-fx)/dx); //https://en.wikipedia.org/wiki/Numerical_differentiation
    }

    return 0;
}

int newton_num(double (*f)(gsl_vector* x),gsl_vector* x_start, double acc, double eps) {
    int n = x_start->size, iterations = 0;

    gsl_vector* grad = gsl_vector_alloc(n);
    gsl_vector* grad_z = gsl_vector_alloc(n);

    gsl_matrix* H0 = gsl_matrix_alloc(n,n); 

    gsl_vector* Dx = gsl_vector_alloc(n);
    gsl_vector* Dx2 = gsl_vector_alloc(n);
    gsl_vector* S = gsl_vector_alloc(n);

    gsl_vector* y = gsl_vector_alloc(n);
    gsl_vector* z = gsl_vector_alloc(n);

    numerical_gradient(f,x_start,grad,eps);
    double f_x = f(x_start);
    double f_z;

    gsl_matrix_set_identity(H0);
    do {
        iterations++;
        gsl_blas_dgemv(CblasNoTrans,-1,H0,grad,0,Dx);
        if (gsl_blas_dnrm2(Dx) < eps*gsl_blas_dnrm2(x_start) ||gsl_blas_dnrm2(grad) < acc) break;

        double s = 1;

        do {
            gsl_vector_memcpy(z,x_start);
            gsl_vector_scale(S,s);
            gsl_vector_memcpy(S,Dx);
            gsl_vector_add(z,S);
            f_z = f(z);

            double Sgrad; 
            gsl_blas_ddot(S,grad,&Sgrad);
            double alpha = 0.01;

            if (f_z < f_x+alpha*Sgrad) break;
            if (s < eps) {
                gsl_matrix_set_identity(H0);
                break;
            }

            s/=2.0;
            gsl_vector_scale(Dx,1./2);
        } while (1); //until break

        numerical_gradient(f,z,grad_z,eps);
        gsl_vector_memcpy(y,grad_z);

        gsl_blas_daxpy(-1,grad,y); // y = gradient of z - grad of y
        gsl_vector_memcpy(Dx2,Dx);
        gsl_blas_dgemv(CblasNoTrans,-1,H0,y,1,Dx2);

        double Sy;
        gsl_blas_ddot(Dx,y,&Sy);

        if (fabs(Sy) > 1e-12) { 
            gsl_blas_dger(1.0/Sy,Dx2,Dx,H0); // equation 13
        }

        gsl_vector_memcpy(x_start,z);
        gsl_vector_memcpy(grad,grad_z);
        f_x = f_z;
    } while(1); //until break

    gsl_vector_free(grad);
    gsl_vector_free(grad_z);
    gsl_matrix_free(H0);
    gsl_vector_free(Dx);
    gsl_vector_free(Dx2);
    gsl_vector_free(S);
    gsl_vector_free(y);
    gsl_vector_free(z);
    return iterations;
}
