#include"header.h"

ann* ann_alloc(int number_of_hidden_neurons, double(*activation_function)(double), double(*activation_function_derivative)(double)) {
  ann *network = (ann*)malloc(sizeof(ann));
  network->n = number_of_hidden_neurons;
  network->f = activation_function;
  network->df = activation_function_derivative;
  network->F =0;
  network->dF =0;
  network->data = gsl_vector_alloc(number_of_hidden_neurons*3);
  return network;
} 

void ann_free(ann* network) {
  gsl_vector_free(network->data);
  free(network);
}

void ann_feed_forward(ann* network, double x) {
  double sum_F = 0;
  double sum_dF = 0;

  for(int i=0; i<network->n; i++) {
    double a = gsl_vector_get(network->data,3*i+0);
    double b = gsl_vector_get(network->data,3*i+1);
    double w = gsl_vector_get(network->data,3*i+2);
    sum_F += network->f((x-a)/b)*w;
    sum_dF += network->df((x-a)/b)*w/b;
  }

  network->F = sum_F;
  network->dF = sum_dF;
  //return sum_dF;
}

void ann_train(ann* network, gsl_vector* xs, double(*fxy)(double,double), double x0, double y0) {
  
  double cost(gsl_vector* X) {

    gsl_vector_memcpy(network->data,X);
    double c = 0;
    double F,dF;
    ann_feed_forward(network,x0);

    double F0 = network->F;

    c += xs->size*fabs(F0-y0)*fabs(F0-y0);

    for(int i=0; i<xs->size; i++) {
      double x = gsl_vector_get(xs,i);
      ann_feed_forward(network,x);
      F = network->F;
      dF = network->dF;

      c += fabs(dF-fxy(x,F))*fabs(dF-fxy(x,F));
    }
    return c/xs->size;
  }
 
  int n = 3*network->n; //since we have 3 paramaters each TIMES 3 hidden nodes
  gsl_vector* X = gsl_vector_alloc(n); // X in param space
  gsl_vector_memcpy(X,network->data);
  
  newton_num(cost,X,1e-4,1e-8);

  gsl_vector_memcpy(network->data,X);
  gsl_vector_free(X);
}
