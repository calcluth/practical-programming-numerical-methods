#include"header.h"


//DEFINE OUR ACTIVATION FUNCTIONS
double act_func(double x) {
	return x*exp(-x*x); //guassian wavelet
}
double act_func_derivative(double x) {
	return exp(-x*x)*(1-2*x*x); //guassian wavelet deriv.
}

//DEFINE FUNCS TO INTEGRATE!
double logistic_func(double x, double y) {
	return y*(1-y);
}
double gaussian_func(double x, double y) {
	return -x*y;
}

//DEFINE ANALYTIC SOLUIONS TO ABOVE FUNCS (FOR COMPARISON)
double logistic_func_actual(double x, double x0, double y0) {
	double A = exp(x0)/y0-exp(x0);
	return exp(x)/(A+exp(x));
}
double gaussian_func_actual(double x, double x0, double y0) {
	double A = y0/exp(-x0*x0/2.0);
	return A*exp(-x*x/2.0);
}

int main() {
	int N = 6; //hidden neurons
	
	ann* network = ann_alloc(N,&act_func,&act_func_derivative);

	double a = -5, b = 5;
	double x, y;
	int points = 20;


	//FOR LOGISITIC FUNC
	double x0 = 0, y0 = 0.5;

	gsl_vector* xs = gsl_vector_alloc(points);
	//gsl_vector* ys = gsl_vector_alloc(points);

	for(int i=0;i<points;i++) {
		x = a+(b-a)*i/(points-1);
		gsl_vector_set(xs,i,x);
		printf("%lg\t%lg\n",x,logistic_func_actual(x,x0,y0));
	}

	///index 0
	printf("\n\n");

	for(int i=0; i<N; i++) {
		gsl_vector_set(network->data,3*i+0,a+(b-a)*i/(network->n-1)); //a_i
		gsl_vector_set(network->data,3*i+1,1); //b_i
		gsl_vector_set(network->data,3*i+2,1); //w_i
	}

	ann_train(network, xs, &logistic_func, x0, y0);


	double neural_x, F, dF;

	///index 1
	printf("\n\n");
	for(neural_x = a; neural_x < b; neural_x += 0.1) {
		ann_feed_forward(network, neural_x);
		printf("%lg\t%lg\n",neural_x,network->F);
	}

	printf("\n\n");

	///index 2
	ann_free(network);
	network = ann_alloc(N,&act_func,&act_func_derivative);

	x0 = 0, y0 = 1.0;
	for(int i=0;i<points;i++) {
		x = a+(b-a)*i/(points-1);
		gsl_vector_set(xs,i,x);
		printf("%lg\t%lg\n",x,gaussian_func_actual(x,x0,y0));
	}
	printf("\n\n");

	for(int i=0; i<N; i++) {
		gsl_vector_set(network->data,3*i+0,a+(b-a)*i/(network->n-1)); //a_i
		gsl_vector_set(network->data,3*i+1,1); //b_i
		gsl_vector_set(network->data,3*i+2,1); //w_i
	}

	ann_train(network, xs, &gaussian_func, x0, y0);

	printf("\n\n");
	for(neural_x = a; neural_x < b; neural_x += 0.1) {
		ann_feed_forward(network, neural_x);
		printf("%lg\t%lg\n",neural_x,network->F);
	}
	///index 3

	ann_free(network);

	gsl_vector_free(xs);
}
