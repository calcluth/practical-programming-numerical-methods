1) How are arguments passed to functions in C: by-value or by-reference? And what does that mean? 

   they are passed by value. the receiving function wil get copies of all the values and doesn't have a direct way to alter the original variables.

2) The value of *(&x) will just be x, so in this case it is 1.23. NULL is a null pointer constant which is a value reserved for indicating that a pointer doesn't refer to a valid object

3) The local variables are destroyed at the end of the function

4) a static variable is one which has a lifetime of the entire run of the program (ie wont be destoryed at the end of a function!)

5) a) i=1 because you set i=1 and then call a function which sets the local variable i to 0 only
   b) i=0 because you pass the pointer to var i and then set the var at address of i to 0
   c) i=1 because you set the address of local variable to NULL only

6) ii - when you pass an array to a function in c, the array will always be converted to a pointer of the first element of the array

7) you cannot work out the size of an array inside a function. It just gives a pointer to the first element so the size is lost

8) stack overflow = "stack smashing detected" detected that there is gonna be a segmentation fault

9) Declaring a 
   i)  static array inside a function, the function can return it (as a pointer);
   ii) variable-length array inside a function, the function cannot return it;
   iii) dynamic array inside a function, the function can return it. (malloc)

10) the program will print:
    i=0
    i=1
    i=2
