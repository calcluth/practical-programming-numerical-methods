#include<stdio.h>
#include<stdlib.h>
#include"komplex.h"

void komplex_print (char *s, komplex a) {
	printf ("%s (%g,%g)\n", s, a.re, a.im);
}

komplex komplex_new (double x, double y) {
	komplex z = { x, y };
	return z;
}

void komplex_set (komplex* z, double x, double y) {
	(*z).re = x;
	(*z).im = y;
}

komplex komplex_add (komplex a, komplex b) {
	komplex result = { a.re + b.re , a.im + b.im };
	return result;
}

komplex komplex_sub (komplex a, komplex b) {
        komplex result = { a.re - b.re , a.im - b.im };
        return result;
}

int komplex_equal(komplex a, komplex b,double epsilon_re, double epsilon_im) {
	if(abs(a.re-b.re)<epsilon_re && abs(a.im-b.im)<epsilon_im) {
		return 1;
	}
	return 0;
}

/* ... */
