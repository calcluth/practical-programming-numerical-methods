#include<stdio.h>
#include<stdlib.h>
#include<assert.h>
#include"nvector.h"


int double_equal(double a, double b,double eps) {
	if(abs(a)-abs(b)<eps){
		return 1;
	}
	return 0;
}
/* ... */

nvector* nvector_alloc(int n){
  nvector* v = malloc(sizeof(nvector));
  (*v).size = n;
  (*v).data = malloc(n*sizeof(double));
  if( v==NULL ) fprintf(stderr,"error in nvector_alloc\n");
  return v;
}

void nvector_free(nvector* v){ free(v->data); free(v);}

void nvector_set(nvector* v, int i, double value){
	assert( 0 <= i && i < (*v).size );
	(*v).data[i]=value;
}

double nvector_get(nvector* v, int i){return (*v).data[i]; }

void nvector_add (nvector* a, nvector* b) {
	for(int i = 0; i < a->size; ++i) {
    	a->data[i] = a->data[i] + b->data[i];
	}
}
void nvector_print (char* s, nvector* v) {
	printf("[");
	for(int i = 0; i < v->size; ++i) {
    	printf(" %g ",v->data[i]);
	}
	printf("]\n");
}
int nvector_equal (nvector* a, nvector* b, double eps) {
	for(int i = 0; i < a->size; ++i) {
    	if(!double_equal(a->data[i],b->data[i],eps)) {
    		return 0;
    	}
	}
	return 1;
}
