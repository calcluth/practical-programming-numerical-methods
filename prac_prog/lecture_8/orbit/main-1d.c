#include <stdio.h>
#include <gsl/gsl_errno.h>
#include <gsl/gsl_matrix.h>
#include <gsl/gsl_odeiv2.h>

int
func (double t, const double y[], double dydx[],void *params)
{
  (void)(t);
  dydx[0] = y[0]*(1-y[0]);
  return GSL_SUCCESS;
}

int main(){
	gsl_odeiv2_system sys = {func, NULL, 1};



	gsl_odeiv2_driver * d = gsl_odeiv2_driver_alloc_y_new (&sys, gsl_odeiv2_step_rk8pd, 1e-6, 1e-6, 0.0);
 	int i;
  	double x = 0.0, x1 = 3.0;
  	double y = 0.5;



	for (i = 1; i <= 100; i++)
	{
	  double xi = i * x1 / 100.0;
	  int status = gsl_odeiv2_driver_apply (d, &x, xi, &y);

	  if (status != GSL_SUCCESS)
	{
	  printf ("error, return value=%d\n", status);
	  break;
	}

	  printf ("%.5e %.5e\n", x, y);
	}

	gsl_odeiv2_driver_free (d);
	return 0;

}
