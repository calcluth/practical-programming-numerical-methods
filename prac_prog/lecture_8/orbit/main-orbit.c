#include <stdio.h>
#include <math.h>
#include<unistd.h> /* short comman-line options */
#include<getopt.h>
#include <gsl/gsl_errno.h>
#include <gsl/gsl_matrix.h>
#include <gsl/gsl_odeiv2.h>


int
func (double t, const double y[], double yprime[],
      void *params)
{
	(void)(t);
	double epsilon = *(double *)params;
	yprime[0] = y[1];
	yprime[1] = epsilon*y[0]*y[0]-y[0]+1;
	return GSL_SUCCESS;
}

int
main (int argc, char **argv)
{

	double epsilon = 0,uprime=0;

	while(1) 
	{
		struct option long_options[] =
		{
			{"epsilon", required_argument, NULL, 'e'},
			{"uprime" , required_argument, NULL, 'p'},
			{0, 0, 0, 0}
		};
		int opt = getopt_long (argc, argv, "e:p:", long_options, NULL);
		if( opt == -1 ) break;
		switch (opt) {
			case 'e': epsilon = atof (optarg); break;
			case 'p': uprime  = atof (optarg); break;
			default:
				fprintf (stderr, "Usage: %s --epsilon epsilon --uprime uprime\n", argv[0]);
				exit (EXIT_FAILURE);
		}

	}

	gsl_odeiv2_system sys = {func, NULL, 2, &epsilon};

	gsl_odeiv2_driver * d = 
	gsl_odeiv2_driver_alloc_y_new (&sys, gsl_odeiv2_step_rk8pd, 1e-6, 1e-6, 0.0);
	int i;
	double phi = 0.0, phi1 = 30*M_PI;
	double y[2] = {1.0,uprime};

	printf ("e=%g\n", epsilon);

	for (i = 1; i <= 2000; i++)
	{
		double phii = i * phi1 / 2000.0;
		int status = gsl_odeiv2_driver_apply (d, &phi, phii, y);

		if (status != GSL_SUCCESS)
		{
			printf ("error, return value=%d\n", status);
			break;
		}

		printf ("%.5e %.5e\n", phii, y[0]);
	}

  gsl_odeiv2_driver_free (d);
  return 0;
}