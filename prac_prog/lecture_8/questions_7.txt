1)  To allocate a vector: gsl_vector * gsl_vector_alloc(size_t n)
	it creates a vector of length n and returns a pointer to the new vector struct
	also include gsl_vector.h
    To allocate a matrix: gsl_matrix * gsl_matrix_alloc(size_t n1, size_t n2)
	creates a matrix of size n1 rows and n2 cols	
	also include gsl_matrix.h
	
2) To free vector v use: void gsl_vector_free(gsl_vector * v)
   To free matrix m use: void gsl_matrix_free(gsl_matrix * m)
   You must free the matrix and vector memories manually

3) To add matrix b to matrix a use: int gsl_vector_add(gsl_vector * a, const gsl_vector * b)
   or use: gsl_blas_daxpy(double alpha, const gsl_vector * x, gsl_vector * y) for y=y+a*x

4) To multiply matrix A with vector use: int gsl_blas_dgemv(CBLAS_TRANSPOSE_t TransA, double alpha, const gsl_matrix * A, const gsl_vector * x, double beta, gsl_vector * y)
	where y := alpha*A*x + beta*y

5) To add matrix a to b:  int gsl_matrix_add(gsl_matrix * a, const gsl_matrix * b)

6) matrix multiply: int gsl_blas_dgemm(CBLAS_TRANSPOSE_t TransA, CBLAS_TRANSPOSE_t TransB, double alpha, const gsl_matrix * A, const gsl_matrix * B, double beta, gsl_matrix * C)

7) To solve Ax=b use: int gsl_linalg_LU_solve(const gsl_matrix * LU, const gsl_permutation * p, const gsl_vector * b, gsl_vector * x)
	also include gsl_linalg.h

  or find inverse of A: int gsl_linalg_LU_invert (const gsl_matrix * LU, const gsl_permutation * p, gsl_matrix * inverse)
  and then multiply the inverse with the vector b: int gsl_blas_dgemv(CBLAS_TRANSPOSE_t TransA, double alpha, const gsl_matrix * A, const gsl_vector * inverse, double 1, gsl_vector * y)

8) int gsl_blas_dgemm(CBLAS_TRANSPOSE_t TransA, CBLAS_TRANSPOSE_t TransB, double alpha, const gsl_matrix * A, const gsl_matrix * B, double beta, gsl_matrix * C)
	Matrix multiplication: C := alpha*op( A )*op( B ) + beta*C
	TransA(B) specifies whether the matrix A (B) should be transposed
	alpha specifies alpha constant
	A(B) is matrix A(B)
	beta is cosntant before C
	C is original matrix C

9) destroyed since it has to use pre-existing memory

10)
	typedef struct
	{
	  int (*function) (double t, const double y[], double dydt[], void *params);
	  int (*jacobian) (double t, const double y[], double *dfdy, double dfdt[],
		           void *params);
	  size_t dimension;
	  void *params;
	}
	gsl_odeiv2_system;

11) int gsl_eigen_symmv_sort(gsl_vector * eval, gsl_matrix * evec, gsl_eigen_sort_t sort_type)

12) above function with GSL_EIGEN_SORT_VAL_ASC or GSL_EIGEN_SORT_ABS_ASC
