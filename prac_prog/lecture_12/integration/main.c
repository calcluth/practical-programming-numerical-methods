#include <stdio.h>
#include <math.h>
#include <gsl/gsl_math.h>
#include <gsl/gsl_integration.h>
#include<assert.h>


double my_hamiltonian_f(double x, void * p)
{

	return (2/sqrt(M_PI))*exp(-x*x);
}

double calculate_energy(double x) {
	gsl_integration_workspace * w = gsl_integration_workspace_alloc (1000);

	double hamiltonian_res, hamiltonian_error;

	gsl_function H;
	H.function = &my_hamiltonian_f;

	gsl_integration_qag (&H, 0, x, 0, 1e-7, 1000, 1, w, &hamiltonian_res, &hamiltonian_error);


	gsl_integration_workspace_free (w);
	return hamiltonian_res;
}


int integrate_question_two(double a, double b, double dx) {
	for(double x = a; x<=b; x+=dx) {
		printf("%lg \t %lg\n",x,calculate_energy(x));
	}
	return 0;
}

int
main (int argc, char **argv)
{
	assert(argc == 4);
	double a = atof(argv[1]);
	double b = atof(argv[2]);
	double dx = atof(argv[3]);
	//integrate_question_one();
	integrate_question_two(a,b,dx);

	return 0;
}