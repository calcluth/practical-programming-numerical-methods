#include <stdio.h>
#include <math.h>
#include <gsl/gsl_math.h>
#include <gsl/gsl_integration.h>
#include<gsl/gsl_errno.h>
#include<gsl/gsl_multiroots.h>
#include<gsl/gsl_odeiv2.h>
#include<assert.h>


int
radial_wave (double x, const double y[], double yprime[],
      void *params)
{
	(void)(x);
	double epsilon = *(double *)params;
	yprime[0] = y[1];
	yprime[1] = -2*(epsilon+1/x)*y[0];
	return GSL_SUCCESS;
}



double F_e(double e, double r){
	const double min_r = 1e-3;

	if(r<min_r) { //before we approximate
		return r-r*r;
	}

	gsl_odeiv2_system system;
	system.function = radial_wave;
	system.jacobian = NULL;
	system.dimension = 2;
	system.params = (void*)&e;

	gsl_odeiv2_driver* driver = 
		gsl_odeiv2_driver_alloc_y_new (&system, gsl_odeiv2_step_rkf45, 1e-6, 1e-6, 1e-3);

	double t=min_r;
	double y[] = {t-t*t, 1-2*t};

	int status = gsl_odeiv2_driver_apply (driver, &t, r, y);
	if (status != GSL_SUCCESS) fprintf (stderr,"F_e: odeiv2 error: %d\n", status);

	gsl_odeiv2_driver_free (driver);

	return y[0];
}

int multiroot_func(const gsl_vector *x, void *params, gsl_vector *f){
	double e=gsl_vector_get(x,0);
	assert(e<0);
	double rmax=*(double*)params;
	double fval=F_e(e,rmax);
	gsl_vector_set(f,0,fval);
	return GSL_SUCCESS;
}

int main(void) {
	double rmax=8;
	int dim = 1;

	gsl_multiroot_fsolver * solver = gsl_multiroot_fsolver_alloc (gsl_multiroot_fsolver_broyden, dim);

	gsl_multiroot_function F = {.f=multiroot_func, .n=dim, .params=(void*)&rmax};

	gsl_vector *x = gsl_vector_alloc(dim);
	gsl_vector_set(x,0,-1);

	gsl_multiroot_fsolver_set(solver, &F, x);

	int status;
	int iter=0;

	const double epsabs=1e-3;

	do {
		iter++;
		status = gsl_multiroot_fsolver_iterate(solver);
		if(status) break;

		status = gsl_multiroot_test_residual(solver->f,epsabs);
		if(status==GSL_SUCCESS) fprintf(stderr,"#converged\n");


		fprintf(stderr,"iter= %3i ",iter); //from dimitry
		fprintf(stderr,"e= %10g ",gsl_vector_get(solver->x,0));
		fprintf(stderr,"f(rmax)= %10g ",gsl_vector_get(solver->f,0));
		fprintf(stderr,"\n");


	} while( status == GSL_CONTINUE && iter < 100);

	double e=gsl_vector_get(solver->x,0);

	printf("# rmax e\n");
	printf("# %g %g\n",rmax,e);
	printf("\n\n");

	printf("# r, F_e(e,r), exact\n");
//	for(double r=0; r<=rmax; r+=rmax/64) printf("%g %g %g\n",r,Fe(e,r),r*exp(-r)*Fe(e,1)*exp(1));
	for(double r=0; r<=rmax; r+=rmax/64) printf("%g %g %g\n",r,F_e(e,r),r*exp(-r));

	gsl_multiroot_fsolver_free(solver);
	gsl_vector_free(x);
	return EXIT_SUCCESS;
}