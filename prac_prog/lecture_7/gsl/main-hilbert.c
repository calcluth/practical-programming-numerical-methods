#include<gsl/gsl_linalg.h>
#include<gsl/gsl_blas.h>
#include <gsl/gsl_math.h>
#include <gsl/gsl_eigen.h>
#include<stdio.h>
#include<stdlib.h>
#include<math.h>
void matrix_print(const char* s,const gsl_matrix* m){
	printf("%s",s); printf("\n");
	for(int j=0;j<m->size2;j++){
		for(int i=0;i<m->size1;i++)
			printf("%8.3g ",gsl_matrix_get(m,i,j));
		printf("\n");
		}
	}
void vector_print(const char* s,const gsl_vector* v){
	printf("%s",s); printf("\n");
	for(int i=0;i<v->size;i++)
		printf("%8.3g ",gsl_vector_get(v,i));
	printf("\n");
	}
double hilbert_entry(int i, int j) {
	return 1.0/(i+j+1);
}
int main(){
	int n=4;
	gsl_matrix* M=gsl_matrix_calloc(n,n);

	for(int i=0;i<n;i++)
	for(int j=0;j<n;j++)
		gsl_matrix_set(M,i,j,hilbert_entry(i,j));

	gsl_vector *eval = gsl_vector_alloc (4);
  	gsl_matrix *evec = gsl_matrix_alloc (4, 4);

  	gsl_eigen_symmv_workspace * w = gsl_eigen_symmv_alloc (4);

	gsl_eigen_symmv (M, eval, evec, w);

	gsl_eigen_symmv_free (w);

	gsl_eigen_symmv_sort (eval, evec, 
	                    GSL_EIGEN_SORT_ABS_ASC);

	matrix_print("matrix M=",M);
	matrix_print("eigenvectors =",evec);
	vector_print("eigenvals =",eval);
	return 0;
}
