#include"header.h"

double square_root(double x);

int
main ()
{
	/*assert(argc == 4);*/

	double my_sqrt, math_sqrt;

	double a = 0;
	double b = 20;
	double dx = 0.2;

	for(double x = a; x<=b; x+=dx) {
		my_sqrt = square_root(x);
		math_sqrt = sqrt(x);
		printf("%lg\t%lg\t%lg\n",x,my_sqrt,math_sqrt);		
	}
	return 0;
} 