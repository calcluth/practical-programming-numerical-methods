#include"header.h"

#define REL_ERR 1E-8 //smaller the number, the more accurate our root will be

struct root_func_params { double r; };

double root_func (double x, void * p) { //x^2 - r function to be passed to root solver
	struct root_func_params * params = (struct my_f_params *)p;
	double r = (params->r);

	return  x*x-r;
}

double square_root(double x) {
	assert(x>=0);

	if(x == 0) return 0;
	if(x == 1) return 1;
	
	if(x<1) { x=1/x; return 1/square_root(x);}
	if(x>=4) return 2*square_root(x/4);

	assert(x > 1 && x < 4); //minimum range we can reduce to with integers

	int status;
	double r = 0;
	int i =0, max_i = 100;
	double x_lo=1, x_hi=4;

	const gsl_root_fsolver_type * T = gsl_root_fsolver_bisection;
	gsl_root_fsolver * s = gsl_root_fsolver_alloc (T);

	gsl_function F;
	struct root_func_params params = { x };

	F.function = &root_func;
	F.params = &params;

	if(x < 1) x_hi=1;	

	gsl_root_fsolver_set (s, &F, x_lo, x_hi);

	do
	{
		i++;
		status = gsl_root_fsolver_iterate (s);
		r = gsl_root_fsolver_root (s);

		x_lo = gsl_root_fsolver_x_lower (s);
		x_hi = gsl_root_fsolver_x_upper (s);

		status = gsl_root_test_interval (x_lo, x_hi,0,REL_ERR);
	}
	while (status == GSL_CONTINUE && i < max_i);

	gsl_root_fsolver_free (s);
	return r;
}