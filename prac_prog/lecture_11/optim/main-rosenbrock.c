#include <stdlib.h>
#include <stdio.h>
#include <gsl/gsl_vector.h>
#include <gsl/gsl_multimin.h>

struct rparams
  {
    double a;
    double b;
  };

double rosenbrock_f (const gsl_vector *v, void *params)
{
  double x, y;
  double *p = (double *)params;
  
  x = gsl_vector_get(v, 0);
  y = gsl_vector_get(v, 1);
 
  return p[0] * (1.0-x)*(1.0-x)+p[1]*(y-x*x)*(y-x*x);
}

int
print_state (size_t iter, gsl_multimin_fminimizer * s)
{
  printf ("iter = %3lu x = % .3f % .3f "
          "f(x) = %10.5f \n",
          iter,
          gsl_vector_get (s->x, 0),
          gsl_vector_get (s->x, 1),
          s->fval);
  return 0;
}

int
main (void)
{

  double par[2] = {1.0, 100.0};

  const gsl_multimin_fminimizer_type *T =  gsl_multimin_fminimizer_nmsimplex2;
  gsl_multimin_fminimizer *s = NULL;
  gsl_vector *ss, *x;
  gsl_multimin_function minex_func;

  int status;
  size_t iter = 0;
  double size;


  //start point
  x = gsl_vector_alloc(2);
  gsl_vector_set(x,0,0);
  gsl_vector_set(x,1,0);


  // initial step size
  ss = gsl_vector_alloc(2);
  gsl_vector_set_all(ss,1.0);

  // initialise method

  minex_func.n = 2;
  minex_func.f = rosenbrock_f;
  minex_func.params = par;

  s = gsl_multimin_fminimizer_alloc(T, 2);
  gsl_multimin_fminimizer_set(s, &minex_func, x, ss);

  do
    {
      iter++;
      status = gsl_multimin_fminimizer_iterate (s);

      print_state (iter, s);

      if (status)   /* check if solver is stuck */
        break;

      size = gsl_multimin_fminimizer_size(s);
      status = gsl_multimin_test_size(size,1e-5);

      if(status == GSL_SUCCESS)
      {
        printf("converged to min at \n");
      }

      print_state(iter, s);
    }
  while (status == GSL_CONTINUE && iter < 100);

  printf ("status = %s\n", gsl_strerror (status));

  gsl_vector_free (x);
  gsl_vector_free (ss);
  gsl_multimin_fminimizer_free (s);
  return 0;
}
