#include <stdlib.h>
#include <stdio.h>
#include <gsl/gsl_vector.h>
#include <gsl/gsl_multimin.h>


#define model(t) A*exp(-t/T)+B

struct exp_data
  {
    int n;
    double *t, *y, *e;
  };

double delta (const gsl_vector *v, void *params)
{
  double A, T, B;
  double *t, *y, *e;
  int n;

  struct exp_data *p = (struct exp_data*) params;

  double sum = 0;

  A = gsl_vector_get(v, 0);
  T = gsl_vector_get(v, 1);
  B = gsl_vector_get(v, 2);


  n = p->n;
  t=p->t;
  y=p->y;
  e=p->e;

  for(int i=0; i<n; i++)
  {
    sum += pow((model(t[i])-y[i])/e[i],2);
  }

 
  return sum;
}

int
print_state (size_t iter, gsl_multimin_fminimizer * s)
{
  printf ("iter = %3lu A = % .3f T = % .3f B = % .3f "
          "f(x) = %10.5f \n",
          iter,
          gsl_vector_get (s->x, 0),
          gsl_vector_get (s->x, 1),
          gsl_vector_get (s->x, 2),
          s->fval);
  return 0;
}

int
main (void)
{

  double t[]= {0.47,1.41,2.36,3.30,4.24,5.18,6.13,7.07,8.01,8.95};
  double y[]= {5.49,4.08,3.54,2.61,2.09,1.91,1.55,1.47,1.45,1.25};
  double e[]= {0.26,0.12,0.27,0.10,0.15,0.11,0.13,0.07,0.15,0.09};
  int n = sizeof(t)/sizeof(t[0]);


  struct exp_data params;
  params.t = t;
  params.y = y;
  params.e = e;
  params.n = n;


  const gsl_multimin_fminimizer_type * type =  gsl_multimin_fminimizer_nmsimplex2;
  gsl_multimin_fminimizer *s = NULL;
  gsl_vector *ss, *x;
  gsl_multimin_function minex_func;

  int status;
  size_t iter = 0;
  double size;



  minex_func.n = 3;
  minex_func.f = delta;
  minex_func.params = (void*)&params;


  //start point
  x = gsl_vector_alloc(minex_func.n);
  gsl_vector_set(x,0,3);
  gsl_vector_set(x,1,2);
  gsl_vector_set(x,2,1);

  // step size
  ss = gsl_vector_alloc(minex_func.n);
  gsl_vector_set_all(ss,2);


  // initialise method

  s = gsl_multimin_fminimizer_alloc(type, minex_func.n);
  gsl_multimin_fminimizer_set(s, &minex_func, x, ss);


  do
    {
      iter++;
      status = gsl_multimin_fminimizer_iterate (s);

      //print_state (iter, s);

      if (status)   /* check if solver is stuck */
        break;

      size = gsl_multimin_fminimizer_size(s);
      status = gsl_multimin_test_size(size,1e-5);

      if(status == GSL_SUCCESS)
      {
        //printf("converged to min at \n");
      }

      //print_state(iter, s);
    }
  while (status == GSL_CONTINUE && iter < 100);


  //printf ("status = %s\n", gsl_strerror (status));



  double A = gsl_vector_get (s->x, 0);
  double T = gsl_vector_get (s->x, 1);
  double B = gsl_vector_get (s->x, 2);

  for(int i=0; i<params.n; i++)
  {
    printf("%10.5f %10.5f %10.5f \n", t[i], y[i], model(t[i]));
  }

  gsl_vector_free (x);
  gsl_vector_free (ss);
  gsl_multimin_fminimizer_free (s);
  return 0;
}
