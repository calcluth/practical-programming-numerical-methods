#include"stdio.h"
#include"tgmath.h"
int hello(void);
double besselfn();
int gammafn();
int main(){

	/*QUESTION 1*/
	/*part 1*/
	int n = 5;
	int gamma=gammafn(n);
	printf("gamma of %i =%i\n",n,gamma);

	/*part 2*/
	double x = 0.5;
	double bessel=besselfn(x);
	printf("1st bessel of %g =%g\n",x,bessel);

	/*part 3*/
	double complex a = csqrt(-2);
	printf("sqrt(-2)= %g+%gi\n",creal(a),cimag(a));

	double complex b = cexp(I);
	printf("e^i= %g+%gi\n",creal(b),cimag(b));

	double complex c = cexp(I*M_PI);
	printf("e^{\\pi i}= %g+%gi\n",creal(c),cimag(c));

	double complex d = cpow(I,exp(1));
	printf("i^e= %g+%gi\n",creal(d),cimag(d));



	/*QUESTION 2*/

	float a_f = 0.1111111111111111111111111111;
	printf("Float:       %.25g\n", a_f);
	double a_d = 0.1111111111111111111111111111;
	printf("Double:      %.25lg\n", a_d);
	long double a_ld = 0.1111111111111111111111111111L;
	printf("Long Double: %.25Lg\n", a_ld);

}
