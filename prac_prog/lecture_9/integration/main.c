#include <stdio.h>
#include <math.h>
#include <gsl/gsl_math.h>
#include <gsl/gsl_integration.h>



double
my_f (double x, void * p)
  {
    return  log(x)/sqrt(x);
  }

double my_norm_f(double x, void * p)
{
	double a = *(double *) p;
	return exp(-a*x*x);
}
double my_hamiltonian_f(double x, void * p)
{
	double a = *(double *) p;

	return (-a*a*x*x/2+a/2+x*x/2)*exp(-a*x*x);
}

int integrate_question_one() {
	gsl_integration_workspace * w = gsl_integration_workspace_alloc (1000);

	double result, error;
	double expected = -4.0;

	gsl_function F;
	F.function = &my_f;
	F.params = NULL;

	gsl_integration_qags (&F, 0, 1, 0, 1e-7, 1000, w, &result, &error);

	printf ("result          = % .18f\n", result);
	printf ("exact result    = % .18f\n", expected);
	printf ("estimated error = % .18f\n", error);
	printf ("actual error    = % .18f\n", result - expected);
	printf ("intervals       = %zu\n", w->size);

	gsl_integration_workspace_free (w);

	return 0;
}

double calculate_energy(double alpha) {
	gsl_integration_workspace * w = gsl_integration_workspace_alloc (1000);

	double norm_res, norm_error, hamiltonian_res, hamiltonian_error;

	gsl_function N;
	N.function = &my_norm_f;
	N.params = &alpha;

	gsl_function H;
	H.function = &my_hamiltonian_f;
	H.params = &alpha;

	gsl_integration_qagi (&N, 0, 1e-7, 1000, w, &norm_res, &norm_error);
	gsl_integration_qagi (&H, 0, 1e-7, 1000, w, &hamiltonian_res, &hamiltonian_error);

	double E = hamiltonian_res/norm_res;


	gsl_integration_workspace_free (w);
	return E;
}


int integrate_question_two() {
	for(double a = 0.1; a<=4.0; a+=0.1) {
		printf("%lg \t %lg\n",a,calculate_energy(a));
	}
	return 0;
}

int
main (int argc, char **argv)
{
	//integrate_question_one();
	integrate_question_two();

	return 0;
}