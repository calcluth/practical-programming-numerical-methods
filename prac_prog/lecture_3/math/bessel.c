#include"stdio.h"
#include"tgmath.h"
int gammafn(); /*could just use tgamma()!!*/
int besselfn(double x)
{
	double bessel = sin(x)/pow(x,2)-cos(x)/x;
	return bessel;
}