1) To print all warnings with gcc compiler add flag `-Wunused`
2) == (equal to) != (neq) > (gt) < (lt) >= (greater or equal to) <= (lt or equal to)
3) if(a>b){printf("%i",a)}else{printf("%i",b)};
4) Yes you need to include -lm when including complex.h
5) it will print 1 (as the incrementation is after the calling) on the first line and then 2 (incremented before) on the second line
   it will give a warning `warning: operation on ‘i’ may be undefined`
6) a for loop is written as:
	for(int i = 0;i<10;i++){printf("%i\n",i);}
7) a while loop form the for loop can be written as
	int i = 0;
	while(i<10){
		printf("%i\n",i);
		i++;
	}
8) for(int i = 0;i<10;i++){printf("%i\n",i);}

9) Yes the code is valid but you should add tabs

10) original C had no boolean data type and just used integers instead where 1 = true, 0 = false. C99 provides a boolean type callled _Bool but you need to include stdbool.h and it had constants which are either `true` or `false`
