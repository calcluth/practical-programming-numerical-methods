#include"stdio.h"
#include"float.h"
#include"limits.h"
int equal(double a, double b, double tau, double epsilon);
int main(){
	int i=1; while(i+1>i) {i++;}
	int myIntMax = i;
	
	int j=1; while(j-1<j){j--;}
	int myIntMin = j;

	/* WE CAN ALSO DO IT WITH FOR AND WHILE/DO LOOPS!*/
	/*int max = 1;
	for(int i=1;i+1>i;i++){
		max = i;
	}*/
	
	/*int max = 1;
	do{
		max++;
	} while (max+1>max);
	*/
	
	float MyFME = 1;
	while(1+MyFME!=1){MyFME/=2;}
	MyFME*=2;

	double MyDME = 1;
	while(1+MyDME!=1){MyDME/=2;}
	MyDME*=2;

	long double MyLDME = 1;
	while(1+MyLDME!=1){MyLDME/=2;}
	MyLDME*=2;
	
	/* WE CAN ALSO DO IT WITH FOR AND WHILE/DO LOOPS!*/

	/*
	double e;
	for(e=1; 1+e!=1; e/=2){}
	e*=2;
	*/

	/*
	double e=1;
	do{e/=2}
	while(1+e!=1);
	e*=2;
	*/

	int max = INT_MAX/3;

	float sum_up_float = 0;
	for(int i =1; i<=max;i++){
		sum_up_float+=1.0f/i;
	}

	float sum_down_float = 0;
	for(int i =max; i>=1;i--){
		sum_down_float+=1.0f/i;
	}

	double sum_up_double = 0;
	for(int i =1; i<=max;i++){
		sum_up_double+=1.0f/i;
	}

	double sum_down_double = 0;
	for(int i =max; i>=1;i--){
		sum_down_double+=1.0f/i;
	}


	printf("my max int = %i vs INT_MAX = %i\n",myIntMax,INT_MAX);
	printf("my min int = %i vs INT_MIN = %i\n",myIntMin,INT_MIN);
	printf("my float machine epsilon = %f vs FLT_EPSILON %f\n",MyFME,FLT_EPSILON);
	printf("my double machine epsilon = %g vs DBL_EPSILON %g\n",MyDME,DBL_EPSILON);
	printf("my long double machine epsilon = %Lg vs DBL_EPSILON %Lg\n",MyLDME,LDBL_EPSILON);
	printf("sum_up_float = %.20f and sum_down_float = %.20f\n\n",sum_up_float,sum_down_float);
	printf("The reason for this difference is that when summing down we are initiating the float to have a high precision whereas the otherway round we begin at a low precision and the high precision terms at the end will be ignored. The series converges as a function of max.\n\n");
	printf("The sum has already converged by MAX_INT/3 since we are already beyond the precision of the float.\n\n");
	printf("sum_up_double = %.30g and sum_down_double = %.30g\n\n",sum_up_double,sum_down_double);
printf("There is a difference again however of a much smaller magnitude. About 10^-6. This is because the precision is much higher.");

	printf("I created a function equal() which gives whether a and b are equal to relative or absolute position.\n");
	printf("For example equal(2,1,0.1,0.1) returns %i\n",equal(2,1,0.1,0.1));

}
