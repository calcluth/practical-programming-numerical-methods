my max int = 2147483647 vs INT_MAX = 2147483647
my min int = -2147483648 vs INT_MIN = -2147483648
my float machine epsilon = 0.000000 vs FLT_EPSILON 0.000000
my double machine epsilon = 2.22045e-16 vs DBL_EPSILON 2.22045e-16
my long double machine epsilon = 1.0842e-19 vs DBL_EPSILON 1.0842e-19
sum_up_float = 15.40368270874023437500 and sum_down_float = 18.80791854858398437500

The reason for this difference is that when summing down we are initiating the float to have a high precision whereas the otherway round we begin at a low precision and the high precision terms at the end will be ignored. The series converges as a function of max.

The sum has already converged by MAX_INT/3 since we are already beyond the precision of the float.

sum_up_double = 20.9661660441635433471674332395 and sum_down_double = 20.9661660389832533724074892234

There is a difference again however of a much smaller magnitude. About 10^-6. This is because the precision is much higher.I created a function equal() which gives whether a and b are equal to relative or absolute position.
For example equal(2,1,0.1,0.1) returns 1