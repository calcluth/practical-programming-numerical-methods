# Practical Programming and Numerical Methods 2018
## Cameron Calcluth

I have created this file using vi. This repository contains my exercises for the course *Practical Programming and Numerical Methods* completed throughout the semester. The course can be found: http://86.52.112.181/~fedorov/prog/

The directory structure is as follows

- final_exam (Numerical Methods Exam: ANN solving ODE)
- num_meth (All numerical methods exercises)
- prac_prog (lectures notes, question answers, and exercises organised by lecture)

See report.pdf in final_exam for results
